CREATE TABLE user_role (user_id int8 not null, roles varchar(255));

ALTER TABLE users RENAME COLUMN email TO username;
ALTER TABLE users ADD COLUMN active boolean;
ALTER TABLE users ALTER COLUMN active SET NOT NULL;
ALTER TABLE user_role ADD FOREIGN KEY (user_id) REFERENCES users;