package ru.innopolis.stc20.diet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import ru.innopolis.stc20.diet.entity.Ingredient;
import ru.innopolis.stc20.diet.entity.User;
import ru.innopolis.stc20.diet.enums.Category;
import ru.innopolis.stc20.diet.repository.IngredientRepository;
import ru.innopolis.stc20.diet.services.FileProcessingServices;
import ru.innopolis.stc20.diet.services.IngredientServices;
import ru.innopolis.stc20.diet.services.UserService;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Oleg_Chapurin
 */
@Controller
@RequestMapping("/ingredients")
@MultipartConfig
public class IngredientsController implements ControllerProcessingIngredients {
    private IngredientServices ingredientServices;
    private FileProcessingServices fileProcessingServices;
    private IngredientRepository ingredientRepository;
    private UserService userService;

    @Autowired
    public IngredientsController(IngredientServices ingredientServices,
                                 FileProcessingServices fileProcessingServices,
                                 IngredientRepository ingredientRepository,
                                 UserService userService) {
        this.ingredientServices = ingredientServices;
        this.fileProcessingServices = fileProcessingServices;
        this.ingredientRepository = ingredientRepository;
        this.userService = userService;
    }


    private Model fillModel(Model model, List<Ingredient> ingredientList, String category, String path_js_css,
                            String descriptionText, Integer caloriesMin, Integer caloriesMax) {
        User user = userService.getUser();
        Category[] categories = Category.values();
        int[] numberPages = new int[ingredientList.size() / 10];
        model.addAttribute("ingredientList", ingredientList);
        if ("".equals(category)) {
            model.addAttribute("categoryFilter", category);
            model.addAttribute("categoryFilterName", category);
        } else {
            model.addAttribute("categoryFilter", Category.valueOf(category).getValue());
            model.addAttribute("categoryFilterName", Category.valueOf(category).name());
        }
        if (user != null){
            model.addAttribute("user_photo", user.getPhoto());
            model.addAttribute("user_name", user.getUsername());
            model.addAttribute("authorized", true);
        } else {
            model.addAttribute("authorized", false);
        }
        model.addAttribute("categories", categories);
        model.addAttribute("path_js_css", path_js_css);
        model.addAttribute("numberPagination", numberPages);
        model.addAttribute("descriptionTextFilter", descriptionText);
        model.addAttribute("caloriesMinFilter", caloriesMin);
        model.addAttribute("caloriesMaxFilter", caloriesMax);
        return model;
    }

    private void savePhoto(HttpServletRequest httpServletRequest, Ingredient ingredient, Part photo) {
        try {
            URL rootPath = httpServletRequest.getServletContext().getResource("/");
            fileProcessingServices.writeDataUploadFileForIngredients(photo, ingredient, rootPath.getPath());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param descriptionText текст поиска в название
     * @param category        категория
     * @param caloriesMin     минимальное количество калорий
     * @param caloriesMax     максимальное количество калорий
     * @return ModelAndViewIngredient - содержит list ингридиентов и path страницы jsp
     */
    @Override
    @GetMapping
    public String showAllIngredientsFilter(Model model,
                                           @RequestParam(value = "descriptionText", defaultValue = "") String descriptionText,
                                           @RequestParam(value = "category", defaultValue = "") String category,
                                           @RequestParam(value = "caloriesMin", defaultValue = "0") int caloriesMin,
                                           @RequestParam(value = "caloriesMax", defaultValue = "0") int caloriesMax,
                                           @RequestParam(value = "page", defaultValue = "0") Integer page,
                                           @RequestParam(value = "size", defaultValue = "10") Integer size) {
        Page<Ingredient> ingredientPage = ingredientServices.getAllIngredientsByFilter(page, size, descriptionText,
                category, caloriesMin, caloriesMax);
        List<Ingredient> ingredientList = ingredientPage.getContent();

        fillModel(model, ingredientList, category, "../../../", descriptionText, caloriesMin, caloriesMax);
        return "allingredients";
    }

    @Override
    @GetMapping("/add")
    public String addIngredient(Model model) {
        model = fillModel(model, new ArrayList<>(), "", "../../", "", 0, 0);
        return "addingredient";
    }

    @Override
    @PostMapping("/add")
    public RedirectView burnIngredient(Model model,
                                       @RequestParam(value = "title", defaultValue = "") String title,
                                       @RequestParam(value = "category", defaultValue = "") String category,
                                       @RequestParam(value = "calories", defaultValue = "0") Integer calories,
                                       @RequestParam(value = "protein", defaultValue = "0") Integer protein,
                                       @RequestParam(value = "fats", defaultValue = "0") Integer fats,
                                       @RequestParam(value = "carbohydrates", defaultValue = "0") Integer carbohydrates,
                                       @RequestParam(value = "dataFile") Part photo, HttpServletRequest httpServletRequest) {
        Ingredient ingredient = ingredientServices.createIngredient(title, category, calories, protein, fats, carbohydrates);
        savePhoto(httpServletRequest, ingredient, photo);
        final long id = ingredientServices.saveIngredient(ingredient);
        return new RedirectView("/ingredients/" + id);
    }

    @Override
    @PostMapping("/{id}")
    public RedirectView updateIngredient(Model model, @PathVariable("id") long id,
                                         @RequestParam(value = "title", defaultValue = "") String title,
                                         @RequestParam(value = "category", defaultValue = "") String category,
                                         @RequestParam(value = "calories", defaultValue = "0") Integer calories,
                                         @RequestParam(value = "protein", defaultValue = "0") Integer protein,
                                         @RequestParam(value = "fats", defaultValue = "0") Integer fats,
                                         @RequestParam(value = "carbohydrates", defaultValue = "0") Integer carbohydrates,
                                         @RequestParam(value = "dataFile") Part photo, HttpServletRequest httpServletRequest) {
        Ingredient ingredient = ingredientRepository.findByIngredientId(id);
        if (ingredient != null) {
            ingredient.setTitle(title);
            ingredient.setCategory(Category.valueOf(category));
            ingredient.setCalories(calories);
            ingredient.setProtein(protein);
            ingredient.setFats(fats);
            ingredient.setCarbohydrates(carbohydrates);
            savePhoto(httpServletRequest,ingredient,photo);
            ingredientRepository.save(ingredient);
        }
        return new RedirectView("/ingredients/" + id);
    }

    @Override
    @GetMapping("/{id}")
    public String showIngredient(Model model, @PathVariable(value = "id") Long id, @RequestParam(value = "editable",
            defaultValue = "false") boolean editable) {
        Ingredient ingredient = ingredientServices.findByIngredientId(id);
        if (ingredient != null) {
            model.addAttribute("path_js_css", "../../");
            model.addAttribute("id", id);
            model.addAttribute("categories", Category.values());
            model.addAttribute("categoriesValue", ingredient.getCategory().name());
            model.addAttribute("categoriesName", ingredient.getCategory().getValue());
            model.addAttribute("title", ingredient.getTitle());
            model.addAttribute("calories", ingredient.getCalories());
            model.addAttribute("protein", ingredient.getProtein());
            model.addAttribute("fats", ingredient.getFats());
            model.addAttribute("carbohydrates", ingredient.getCarbohydrates());
            model.addAttribute("dataFile", ingredient.getPhoto());
            model.addAttribute("editable", editable);
        } else {
            return "404";
        }
        final User user = userService.getUser();
        if (user != null) {
            model.addAttribute("user_photo", user.getPhoto());
            model.addAttribute("user_name", user.getUsername());
            model.addAttribute("authorized", true);
        } else {
            model.addAttribute("authorized", false);
        }

        return editable ? "editingredient" : "showingredient";
    }

    /**
     * @param id переменная из пути
     */
    @Override
    @PostMapping("/{id}/delete")
    public RedirectView deleteIngredient(@PathVariable(value = "id") Long id) {
        ingredientServices.deleteIngredient(id);
        return new RedirectView("/ingredients");
    }
}
