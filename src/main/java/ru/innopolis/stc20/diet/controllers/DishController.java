package ru.innopolis.stc20.diet.controllers;

import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;

public interface DishController {

    String viewDishes(Model model, int currentPage, String[] dishNames,
                      String tags, String calFrom, String calTo,
                      String timeFrom, String timeTo,HttpServletRequest request);

    String editDish(Long id, boolean editable, int page, Model model);

    String removeDish(Long id, Model model);
    String addDish();
}
