package ru.innopolis.stc20.diet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import ru.innopolis.stc20.diet.entity.Comment;
import ru.innopolis.stc20.diet.entity.Dish;
import ru.innopolis.stc20.diet.entity.User;
import ru.innopolis.stc20.diet.repository.DishRepository;
import ru.innopolis.stc20.diet.services.CommentServices;
import ru.innopolis.stc20.diet.services.UserService;

import java.util.List;


/**
 * Обрабатывает все запроссы к странице комментариев
 *
 * @author Oleg_Chapurin
 */
@Controller
@RequestMapping("/")
public class ControllerProcessingRequestsOnPageComments implements ControllerProcessingComments {

    private CommentServices commentServices;
    private DishRepository dishRepository;
    private UserService userService;

    @Autowired
    public ControllerProcessingRequestsOnPageComments(CommentServices commentServices, DishRepository dishRepository,
                                                      UserService userService) {
        this.commentServices = commentServices;
        this.dishRepository = dishRepository;
        this.userService = userService;
    }

    private void fillModel(Dish dish,Model model,User user,long id_subj,int page,int size,String path_js_css){
        if (dish != null) {
            Page<Comment> commentPage = commentServices.getDataForCommentPage(id_subj, page, size);
            List<Comment> commentsList = commentPage.getContent();
            int[] numberPages = new int[commentsList.size() % size];
            model.addAttribute("commentsList", commentsList);
            model.addAttribute("numberPagination", numberPages);
            model.addAttribute("path_js_css", path_js_css);
            model.addAttribute("dish", dish);
            model.addAttribute("textComment", "");
            if(user == null){
                user = new User();
                user.setId(0L);
                model.addAttribute("authorized", false);
            } else {
                model.addAttribute("user_photo", user.getPhoto());
                model.addAttribute("user_name", user.getUsername());
                model.addAttribute("authorized", true);
            }
            model.addAttribute("user", user);
            model.addAttribute("idUser", user.getId());
            model.addAttribute("namePage", "comments");
        } else {
            model.addAttribute("namePage", "dishes");
        }
    }

    /**
     *
     * @param model - Model
     * @param id_subj - id dish subj
     * @param page- number page
     * @return name page
     */
    @Override
    @GetMapping("comments/{id_subj}/{page}")
    public String showAllComments(Model model, @PathVariable("id_subj") long id_subj,
                                  @PathVariable("page") int page) {
        Dish dish = dishRepository.findByDishId(id_subj);
        User user = userService.getUser();
        fillModel(dish,model,user,id_subj,page,10,"../../../");
        return (String) model.getAttribute("namePage");
    }

    /**
     * @param model       - Model
     * @param id_subj     - id dish subj
     * @param textComment - text comment
     * @return name page
     */
    @Override
    @PostMapping("/dishes/{id_subj}/comments")
    public RedirectView creatComment(Model model, @PathVariable("id_subj") long id_subj,
                                     @RequestParam(value = "textComment", required = false) String textComment) {
        Dish dish = dishRepository.findByDishId(id_subj);
        User user = userService.getUser();
        if (dish != null) {
            commentServices.saveComment(textComment, user, dish);
        }
        return new RedirectView("/dishes/" + id_subj + "?editable=false");
    }

    @Override
    @PostMapping("comments/exit")
    public String exitComments() {
        return "dishes";
    }
}
