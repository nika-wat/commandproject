package ru.innopolis.stc20.diet.controllers;

import org.springframework.ui.Model;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Обрабатывает запроссы к странице комментариев
 * @author Oleg_Chapurin
 */
public interface ControllerProcessingComments {

    /**
     *
     * @param model - Model
     * @param id_subj - id dish subj
     * @param page- number page
     * @return name page
     */
    String showAllComments(Model model, long id_subj,int page);

    /**
     * @param model       - Model
     * @param id_subj     - id dish subj
     * @param textComment - text comment
     * @return name page
     */
    RedirectView creatComment(Model model, long id_subj, String textComment);

    String exitComments();
}
