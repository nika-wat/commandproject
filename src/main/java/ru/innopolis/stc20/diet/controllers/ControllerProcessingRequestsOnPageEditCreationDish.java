package ru.innopolis.stc20.diet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.stc20.diet.entity.Dish;
import ru.innopolis.stc20.diet.entity.Ingredient;
import ru.innopolis.stc20.diet.entity.User;
import ru.innopolis.stc20.diet.enums.Category;
import ru.innopolis.stc20.diet.services.DishServices;
import ru.innopolis.stc20.diet.services.UserService;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Oleg_Chapurin
 */
@Controller
@RequestMapping("/newdish")
@MultipartConfig
public class ControllerProcessingRequestsOnPageEditCreationDish implements ControllerProcessingEditCreationDish {
    private DishServices dishServices;
    private UserService userService;

    @Autowired
    public ControllerProcessingRequestsOnPageEditCreationDish(DishServices dishServices,UserService userService) {
        this.dishServices = dishServices;
        this.userService = userService;
    }

    private Model fillModel(Model model, Dish dish, String path_js_css, String category,
                            List<Ingredient> listIngredients) {
        User user = userService.getUser();
        Category[] categories = Category.values();
        model.addAttribute("path_js_css", path_js_css);
        model.addAttribute("dish", dish);
        model.addAttribute("categories", categories);
        if ("" .equals(category)) {
            model.addAttribute("currentCategory", "");
            model.addAttribute("currentCategoryName", "");
        } else {
            model.addAttribute("currentCategory", Category.valueOf(category).getValue());
            model.addAttribute("currentCategoryName", category);
        }
       if(listIngredients != null){
        model.addAttribute("listIngredients", listIngredients);
       } else {
           model.addAttribute("listIngredients", new ArrayList<Ingredient>());
       }
        if (user != null){
            model.addAttribute("user_photo", user.getPhoto());
            model.addAttribute("user_name", user.getUsername());
            model.addAttribute("authorized", true);
        } else {
            model.addAttribute("authorized", false);
        }
        model.addAttribute("namePage", "createdish");
        return model;
    }

    @Override
    @GetMapping
    public String newPage(Model model) {
        Dish dish = dishServices.getDish();
        model = fillModel(model,dish, "../", "",null);
        return (String) model.getAttribute("namePage");
    }

    @Override
    @GetMapping("/{idDish}")
    public String editPage(Model model, @PathVariable("idDish") long idDish) {
        Dish dish = dishServices.getDishById(idDish);
        if (dish != null) {
            model = fillModel(model,dish, "../../", "",null);
            model.addAttribute("map", dishServices.getIndicatorsPer100Grams(dish.getDishId()));
        } else {
            model.addAttribute("namePage", "dishes");
        }
        return (String) model.getAttribute("namePage");
    }

    @Override
    @PostMapping("/exit")
    public String exitDishEditingMode() {
        return "dishes";
    }

    @Override
    @PostMapping("/{idDish}/remove")
    public String removeDish(@PathVariable("idDish") long idDish) {
        dishServices.removeDish(idDish);
        return "dishes";
    }

    @Override
    @PostMapping("/burn/{idDish}")
    public String burnDish(Model model, @PathVariable(value = "idDish", required = false) long idDish,
                           @RequestParam(value = "dishName", defaultValue = "") String dishName,
                           @RequestParam(value = "cookingTime", defaultValue = "0") Integer cookingTime,
                           @RequestParam(value = "recipe", defaultValue = "") String recipe,
                           @RequestParam(value = "tedsSearch", defaultValue = "") String tedsSearch,
                           @RequestParam(value = "dataFile") Part dataFile, HttpServletRequest httpServletRequest) {

        Dish dish = dishServices.getDishById(idDish);
        if (dish != null) {
            try {
                URL rootPath = httpServletRequest.getServletContext().getResource("/");
                dish = dishServices.burnDish(dish, dishName, cookingTime, recipe, tedsSearch, dataFile, rootPath);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            model = fillModel(model,dish, "../../../", "",null);
        } else {
            model.addAttribute("namePage", "dishes");
        }
        return (String) model.getAttribute("namePage");
    }

    @Override
    @PostMapping("/addingredient/{idDish}")
    public String addIngredient(Model model, @PathVariable("idDish") long idDish,
                                @RequestParam(value = "ingredient", defaultValue = "0") Long ingredient,
                                @RequestParam(value = "weightGrams", defaultValue = "0") Integer weightGrams) {
        Dish dish = dishServices.getDishById(idDish);
        if (dish != null) {
            dish = dishServices.addIngredientToDish(ingredient, weightGrams, idDish);
            model.addAttribute("map", dishServices.getIndicatorsPer100Grams(idDish));
            model = fillModel(model,dish, "../../../", "",null);
        } else {
            model.addAttribute("namePage", "dishes");
        }
        return (String) model.getAttribute("namePage");
    }

    @Override
    @PostMapping("/{idDish}/ingredient/{id_ingredient}/{weight}/delete")
    public String removeIngredient(Model model, @PathVariable("idDish") long idDish,
                                   @PathVariable("id_ingredient") long id_ingredient,
                                   @PathVariable("weight") int weight) {
        Dish dish = dishServices.getDishById(idDish);
        if (dish != null) {
            dish = dishServices.removeIngredientFromDish(idDish, id_ingredient, weight);
            model = fillModel(model,dish, "../../../../../../", "",null);
        } else {
            model.addAttribute("namePage", "dishes");
        }
        return (String) model.getAttribute("namePage");
    }

    @Override
    @PostMapping("/categories/{idDish}")
    public String selectCategoriesIngredient(Model model, @PathVariable("idDish") long idDish,
                                             @RequestParam(value = "category", defaultValue = "") String category) {
        List<Ingredient> listIngredients = dishServices.chooseIngredientsByCategory(category);
        Dish dish = dishServices.getDishById(idDish);
        if (dish != null) {
            model = fillModel(model,dish, "../../../", category,listIngredients);
        } else {
            model.addAttribute("namePage", "dishes");
        }
        return "createdish";
    }
}
