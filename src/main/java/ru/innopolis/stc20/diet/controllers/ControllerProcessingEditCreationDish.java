package ru.innopolis.stc20.diet.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

/**
 * @author Oleg_Chapurin
 */
@Controller
public interface ControllerProcessingEditCreationDish {

    String editPage(Model model, long idDish);
    String exitDishEditingMode();
    String removeDish(long idDish);
    String burnDish(Model model, long idDish, String dishName, Integer cookingTime,
                    String recipe, String tedsSearch, Part dataFile, HttpServletRequest httpServletRequest);
    String addIngredient(Model model,long idDish,Long ingredient,Integer weightGrams);
    String removeIngredient(Model model,long id,long id_ingredient,int weight);
    String selectCategoriesIngredient(Model model,long idDish,String category);
    String newPage(Model model);
}
