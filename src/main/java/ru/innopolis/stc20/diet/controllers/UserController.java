package ru.innopolis.stc20.diet.controllers;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import ru.innopolis.stc20.diet.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

public interface UserController {

    String viewInfoUser(Model model);

    String saveInfoUser(@RequestParam("inputGender") String gender,
                        @RequestParam("inputPurpose") String purpose,
                        @RequestParam("inputActivity") String activity,
                        @RequestParam("inputBirthDate") String date,
                        @RequestParam("inputHeight") int height,
                        @RequestParam("inputWeight") double weight,
                        @RequestParam("inputFullName") String fullName,
                        @RequestParam(value = "dataFile") Part photo,
                        Model model, HttpServletRequest httpServletRequest);

    String viewLoginUser(@RequestParam(value = "error", required = false) String error,
                         @RequestParam(value = "logout", required = false) String logout,
                         Model model);

    String saveLoginUser(@RequestParam(value = "error", required = false) String error,
                         @RequestParam(value = "logout", required = false) String login,
                         @RequestParam("email") String email,
                         @RequestParam("password") String password,
                         Model model);

    String viewRegistrationUser();

    String saveRegistrationUser(User user, Model model);

    String changePasswordUser(@RequestParam("inputNewPassword") String newPassword,
                              @RequestParam("inputOldPassword") String oldPassword,
                              Model model);
}
