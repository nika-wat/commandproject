package ru.innopolis.stc20.diet.controllers;

import org.springframework.ui.Model;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

/**
 * @author Oleg_Chapurin
 */
public interface ControllerProcessingIngredients {
    /**
     * @param descriptionText текст поиска в название
     * @param category        категория
     * @param caloriesMin     минимальное количество калорий
     * @param caloriesMax     максимальное количество калорий
     * @return ModelAndViewIngredient - содержит list ингридиентов и path страницы jsp
     */
    String showAllIngredientsFilter(Model model, String descriptionText, String category,
                              int caloriesMin, int caloriesMax, Integer page, Integer size);

    String addIngredient(Model model);
    String showIngredient(Model model, Long id, boolean editable);

    //    String editIngredient(Model model,Long id, String title, String category, Integer calories, Integer protein,
//                            Integer fats, Integer carbohydrates, boolean editable);
    RedirectView updateIngredient(Model model, long id, String title, String category, Integer calories, Integer protein,
                                  Integer fats, Integer carbohydrates, Part photo, HttpServletRequest httpServletRequest);

    RedirectView burnIngredient(Model model, String title, String category, Integer calories, Integer protein,
                                Integer fats, Integer carbohydrates, Part photo, HttpServletRequest httpServletRequest);

    RedirectView deleteIngredient(Long id);
}
