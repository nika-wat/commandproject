package ru.innopolis.stc20.diet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.innopolis.stc20.diet.entity.User;
import ru.innopolis.stc20.diet.enums.Activity;
import ru.innopolis.stc20.diet.enums.Gender;
import ru.innopolis.stc20.diet.enums.Purpose;
import ru.innopolis.stc20.diet.enums.Role;
import ru.innopolis.stc20.diet.exceptions.NotEqualsPasswordException;
import ru.innopolis.stc20.diet.services.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.util.Collections;

@Controller
@RequestMapping("/user")
public class UserControllerImpl implements UserController {
    private static final String PAGE_PERSACC = "persacc";
    private static final String PAGE_LOGIN = "login";

    private UserService userService;

    @Autowired
    public UserControllerImpl(UserService userService) {
        this.userService = userService;
    }

    @Bean
    public PasswordEncoder bcryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @GetMapping("/info")
    public String viewInfoUser(Model model) {
        User user = userService.getUser();
        model.addAttribute("user", user);
        model.addAttribute("year", userService.getNumberOfFullYearsUser());
        model.addAttribute("path_js_css", "../../");
        addSessionAttributes(model);
        return PAGE_PERSACC;
    }

    @Override
    @PostMapping("/info")
    public String saveInfoUser(String gender, String purpose, String activity, String date,
                               int height, double weight, String fullName, Part photo,
                               Model model, HttpServletRequest httpServletRequest) {
        User user = userService.updateUserFields(gender, purpose, activity, date, height, weight,
                fullName, httpServletRequest, photo);
        model.addAttribute("user", user);
        model.addAttribute("year", userService.getNumberOfFullYearsUser());
        model.addAttribute("path_js_css", "../../");
        addSessionAttributes(model);
        return PAGE_PERSACC;
    }

    @Override
    @GetMapping("/login")
    public String viewLoginUser(String error, String logout,Model model) {
        model.addAttribute("error", error != null);
        model.addAttribute("logout", logout != null);
        model.addAttribute("path_js_css", "../../");
        return PAGE_LOGIN;
    }

    @Override
    @PostMapping("/login")
    public String saveLoginUser(String error, String logout, String email, String password,
                                Model model) {
        model.addAttribute("path_js_css", "../../");
        return "dishes";
    }

    @Override
    @GetMapping("/registration")
    public String viewRegistrationUser() {
        return "registration";
    }

    @Override
    @PostMapping("/registration")
    public String saveRegistrationUser(User user, Model model) {
        User userFromDb = userService.getUserUsername(user.getUsername());

        if (userFromDb != null) {
            model.addAttribute("message", "Пользователь с таким логином существует!");
            return "registration";
        }
        model.addAttribute("path_js_css", "../../");
        user.setUsername(user.getUsername());
        user.setPassword(bcryptPasswordEncoder().encode(user.getPassword()));
        user.setActive(true);
        user.setRoles(Collections.singleton(Role.USER));
        userService.saveUser(user);

        return "redirect:/user/login";
    }

    @Override
    @PostMapping("/change-password")
    public String changePasswordUser(String newPassword, String oldPassword, Model model) {
        User user = userService.getUser();
        try {
            user = userService.getUserWithChangePassword(newPassword, oldPassword);
            model.addAttribute("changePassword", true);
        } catch (NotEqualsPasswordException e) {
            model.addAttribute("messageError", "Старый пароль не верный");
        }
        model.addAttribute("user", user);
        model.addAttribute("path_js_css", "../../");
        return PAGE_PERSACC;
    }

    private void addSessionAttributes(Model model) {
        model.addAttribute("genders", Gender.values());
        model.addAttribute("purposes", Purpose.values());
        model.addAttribute("activities", Activity.values());
    }
}
