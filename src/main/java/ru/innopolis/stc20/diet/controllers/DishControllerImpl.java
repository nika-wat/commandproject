package ru.innopolis.stc20.diet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.stc20.diet.entity.Comment;
import ru.innopolis.stc20.diet.entity.Dish;
import ru.innopolis.stc20.diet.entity.User;
import ru.innopolis.stc20.diet.services.CommentServices;
import ru.innopolis.stc20.diet.services.DishServices;
import ru.innopolis.stc20.diet.services.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping(value = {"/", "/dishes"})
public class DishControllerImpl implements DishController {
    private final DishServices dishServices;
    private final UserService userService;
    private final CommentServices commentServices;

    @Autowired
    public DishControllerImpl(DishServices dishServices, UserService userService, CommentServices commentServices) {
        this.dishServices = dishServices;
        this.userService = userService;
        this.commentServices = commentServices;
    }

    @Override
    @GetMapping
    public String viewDishes(Model model, @RequestParam(value = "currentPage", defaultValue = "1") int currentPage,
                             @RequestParam(value = "dishNames", required = false) String[] dishNames,
                             @RequestParam(value = "tags", required = false) String tags,
                             @RequestParam(value = "calFrom", required = false) String calFrom,
                             @RequestParam(value = "calTo", required = false) String calTo,
                             @RequestParam(value = "timeFrom", required = false) String timeFrom,
                             @RequestParam(value = "timeTo", required = false) String timeTo,
                             HttpServletRequest request) {
        int limit = 5;
        Collection<Dish> listDishes = dishServices.getDishesWithParams(dishNames, tags, calFrom, calTo,
                timeFrom, timeTo, limit, (currentPage-1)*limit);
        List<Dish> listAllDishes = dishServices.getAllDishes();
        int rowCount = dishServices.getRowCount(dishNames, tags, calFrom, calTo, timeFrom, timeTo);
        int nOfPages = (int) Math.ceil(rowCount/(float)limit);
        User user = userService.getUser();
        if (user != null){
            model.addAttribute("user_photo", user.getPhoto());
            model.addAttribute("user_name", user.getUsername());
            model.addAttribute("authorized", true);
        } else {
            model.addAttribute("authorized", false);
        }
        model.addAttribute("allDishes", listAllDishes);
        model.addAttribute("dishes", listDishes);
        model.addAttribute("noOfPages", nOfPages);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("calTo", calTo);
        return "dishes";
    }

    @Override
    @GetMapping("/{id}")
    public String editDish(@PathVariable(value = "id") Long id,
                           @RequestParam(value = "editable", defaultValue = "false") boolean editable,
                           @RequestParam(value = "page", defaultValue = "0") int page,
                           Model model) {
        model.addAttribute("path_js_css", "../../../");
        model.addAttribute("dish", dishServices.getDishById(id));
        model.addAttribute("map", dishServices.getIndicatorsPer100Grams(id));

        final User user = userService.getUser();
        if (user != null) {
            model.addAttribute("user_photo", user.getPhoto());
            model.addAttribute("user_name", user.getUsername());
            model.addAttribute("authorized", true);
        } else {
            model.addAttribute("authorized", false);
        }

        fillModel(model, id, page, 10);
        return editable ? "redirect:/newdish/{id}" : "dishEdit";
    }

    @Override
    @PostMapping("/{id}/delete")
    public String removeDish(@PathVariable(value = "id") Long id, Model model) {
        model.addAttribute("path_js_css", "../../");
        dishServices.removeDish(id);
        return "redirect:/dishes";
    }

    @Override
    @GetMapping("/add")
    public String addDish() {
        return "redirect:/newdish";
    }

    private void fillModel(Model model, long id_subj, int page, int size) {
        final Page<Comment> commentPage = commentServices.getDataForCommentPage(id_subj, page, size);
        List<Comment> commentsList = commentPage.getContent();
        int[] numberPages = new int[commentPage.getTotalPages()];
        for (int i = 0; i < numberPages.length; i++) {
            numberPages[i] = i;
        }
        model.addAttribute("commentsList", commentsList);
        model.addAttribute("numberPagination", numberPages);
        model.addAttribute("textComment", "");
        model.addAttribute("namePage", "comments");
    }
}
