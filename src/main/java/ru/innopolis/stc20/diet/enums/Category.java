package ru.innopolis.stc20.diet.enums;


/**
 * Класс-перечисление категорий продуктов
 * @author Veronika Abakumova
 */
public enum Category {
    MEET ("Мясо и птица"),
    FISH ("Рыба и рыбопродукты"),
    GRAINS ("Крупы и бобовые"),
    BREAD ("Хлебобулочные изделия"),
    MILK ("Молочные продукты"),
    FRUIT ("Фрукты, сухофрукты, ягоды"),
    VEGGIES ("Овощи, зелень"),
    NUTS ("Орехи, семечки"),
    EGGS ("Яйца"),
    MUSHROOMS ("Грибы"),
    SUGAR ("Сахар, мед и кондитерские товары"),
    OIL ("Масло, соусы");

    private String value;

    Category (String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Category getByValue(String nameCategory){
        Category[] arrayCategory = Category.values();
        for (Category category:arrayCategory) {
            if (category.getValue().equals(nameCategory)){
                return category;
            }
        }
        return null;
    }
}
