package ru.innopolis.stc20.diet.enums;

public enum Purpose {
    SLIM(0.15),
    NORMAL(0),
    FAT(0.15);

    private double value;

    Purpose(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
