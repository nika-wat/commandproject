package ru.innopolis.stc20.diet.enums;

/**
 * Object type - ingredient, dish
 * @author Oleg_Chapurin
 */
public enum TypeSubj {
    INGREDIENT("I"),
    DISH("D");
    private String typeSubj;

    TypeSubj(String typeSubj) {
        this.typeSubj = typeSubj;
    }

    public String getTypeSubjInStringView(){
        return typeSubj;
    }

    /** Get enum by name type **/
    public static TypeSubj getEnumByNameTypeSubj(String type) {
        TypeSubj[] array = TypeSubj.values();
        for (int i = 0; i < array.length; i++){
            if(array[i].getTypeSubjInStringView().equals(type)){
                return array[i];
            }
        }
        return null;
    }
}
