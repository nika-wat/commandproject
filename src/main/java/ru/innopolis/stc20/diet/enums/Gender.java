package ru.innopolis.stc20.diet.enums;

/**
 * Класс-перечисление.
 * @author Aleksey Danilchik
 * @version 1.0
 */
public enum Gender {
    MALE, FEMALE
}
