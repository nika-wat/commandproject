package ru.innopolis.stc20.diet.enums;

/**
 * Класс-перечисление.
 * @author Aleksey Danilchik
 * @version 1.0
 */
public enum Activity {
    MINIMUM(1.2),
    LIGHT(1.35),
    AVERAGE(1.55),
    HIGH(1.75),
    EXTREMELY(2);

    private double value;

    Activity(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
