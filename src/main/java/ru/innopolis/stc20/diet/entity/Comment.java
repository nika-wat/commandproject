package ru.innopolis.stc20.diet.entity;

import ru.innopolis.stc20.diet.enums.TypeSubj;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.OffsetDateTime;

/**
 * Entity
 * @author Oleg_Chapurin
 */
@Entity
@Table(name = "comment")
public class Comment  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "saying",length = 1000)
    private String text;

    @Column(name = "creation_date")
    private OffsetDateTime creationDate;

    @Column(name = "id_cubj")
    private Long idSubjDiscussions;

    @Column(name = "type_subj")
    private TypeSubj typeSubjDiscussions;

    public Comment(){}

    public Comment(User user, @Size(max = 1000) String text, OffsetDateTime creationDate,
                   Long idSubjDiscussions, TypeSubj typeSubjDiscussions) {
        this.user = user;
        this.text = text;
        this.creationDate = creationDate;
        this.idSubjDiscussions = idSubjDiscussions;
        this.typeSubjDiscussions = typeSubjDiscussions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdUser() {
        return user.getId();
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public OffsetDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(OffsetDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public Long getIdSubjDiscussions() {
        return idSubjDiscussions;
    }

    public void setIdSubjDiscussions(Long idSubjDiscussions) {
        this.idSubjDiscussions = idSubjDiscussions;
    }

    public TypeSubj getTypeSubjDiscussions() {
        return typeSubjDiscussions;
    }

    public void setTypeSubjDiscussions(TypeSubj typeSubjDiscussions) {
        this.typeSubjDiscussions = typeSubjDiscussions;
    }
}
