package ru.innopolis.stc20.diet.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Oleg_Chapurin
 */
@Entity
@Table(name = "tags")
public class Tag  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tag_id")
    private long tagId;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name="DISH_ID")
    private Dish dish;

    @Column(name = "tag")
    private String tag;

    public Tag() {}

    public Tag(Dish dish, String tag) {
        this.dish = dish;
        this.tag = tag;
    }

    public long getTagId() {
        return tagId;
    }

    public void setTagId(long tagId) {
        this.tagId = tagId;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
