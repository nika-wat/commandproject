package ru.innopolis.stc20.diet.entity;

import ru.innopolis.stc20.diet.enums.Category;
import ru.innopolis.stc20.diet.utils.ConvertCategory;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Класс Ingredient
 * @author Veronika Abakumova
 */
@Entity
@Table(name = "ingredients")
public class Ingredient  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long ingredientId;

    @Column(name = "title")
    private String title;

    @Column(name = "category")
    @Convert(converter = ConvertCategory.class)
    private Category category;

    @Column(name = "calories")
    private int calories;

    @Column(name = "protein")
    private int protein;

    @Column(name = "fats")
    private int fats;

    @Column(name = "carbohydrates")
    private int carbohydrates;

    @Column(name = "photo")
    private String photo;

    public Ingredient(){}

    public Ingredient(long ingredientId, String title, Category category, int calories, int protein, int fats,
                      int carbohydrates, String photo) {
        this.ingredientId = ingredientId;
        this.title = title;
        this.category = category;
        this.calories = calories;
        this.protein = protein;
        this.fats = fats;
        this.carbohydrates = carbohydrates;
        this.photo = photo;
    }

    public long getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(long ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public int getProtein() {
        return protein;
    }

    public void setProtein(int protein) {
        this.protein = protein;
    }

    public int getFats() {
        return fats;
    }

    public void setFats(int fats) {
        this.fats = fats;
    }

    public int getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(int carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public String getPhoto() {
        if (photo != null) {
            return photo;
        }
        return "";
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ingredient that = (Ingredient) o;
        return ingredientId == that.ingredientId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ingredientId);
    }
}
