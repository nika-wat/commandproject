package ru.innopolis.stc20.diet.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Класс-сущность для хранения перечня ингредиентов блюда с указанием веса
 *
 * @author Valentina Gorbunova
 */
@Entity
@Table(name = "dish_ingredients")
public class DishIngredients implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dish_ingredients_id")
    private long dishIngredientsId;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "DISH_ID")
    private Dish dish;

    @OneToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "ingredient_id")
    private Ingredient ingredient;

    @Column(name = "weight")
    private int weight;

    public DishIngredients() {
    }

    public DishIngredients(Dish dish, Ingredient ingredient, int weight) {
        this.dish = dish;
        this.ingredient = ingredient;
        this.weight = weight;
    }

    public long getId() {
        return dishIngredientsId;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getCalories() {
            return this.ingredient.getCalories() / 100 * weight;
    }

    public int getProtein() {
            return this.ingredient.getProtein() / 100 * weight;
    }

    public int getFats() {
            return this.ingredient.getFats() / 100 * weight;
    }

    public int getCarbohydrates() {
            return this.ingredient.getCarbohydrates() / 100 * weight;
    }

    public String getPhoto() {
        return ingredient.getPhoto();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DishIngredients that = (DishIngredients) o;
        return this.getId() == that.getId() &&
                Objects.equals(dish, that.dish) &&
                Objects.equals(ingredient, that.ingredient);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getId(), dish, ingredient, weight);
    }
}