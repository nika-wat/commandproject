package ru.innopolis.stc20.diet.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Класс-сущность для хранения блюда
 * @author Valentina Gorbunova
 */
@Entity
@Table(name = "dish")
public class Dish  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dish_id")
    private long dishId;

    @Column(name = "dish_name")
    private String dishName;

    @Column(name = "calories")
    private int calories;

    @Column(name = "protein")
    private int protein;

    @Column(name = "fat")
    private int fat;

    @Column(name = "carbohydrate")
    private int carbohydrate;

    @Column(name = "cooking_time")
    private int cookingTime;

    @OneToMany(mappedBy = "dish",fetch = FetchType.LAZY,cascade =  CascadeType.ALL)
    private List<DishIngredients> ingredients;

    @OneToMany(mappedBy = "dish",fetch = FetchType.EAGER,cascade =  CascadeType.ALL)
    private Set<Tag> tags;

    @Column(name = "recipe")
    private String recipe;

    @Column(name = "photo")
    private String photo;

    public Dish(String dishName, int calories, int protein, int fat, int carbohydrate) {
        this.dishName = dishName;
        this.calories = calories;
        this.protein = protein;
        this.fat = fat;
        this.carbohydrate = carbohydrate;
    }

    public Dish(long dishId, String dishName, int calories, int protein, int fat, int carbohydrate, int cookingTime,
                List<DishIngredients> ingredients, Set<Tag> tags, String recipe, String photo) {
        this.dishId = dishId;
        this.dishName = dishName;
        this.calories = calories;
        this.protein = protein;
        this.fat = fat;
        this.carbohydrate = carbohydrate;
        this.cookingTime = cookingTime;
        this.ingredients = ingredients;
        this.tags = tags;
        this.recipe = recipe;
        this.photo = photo;
    }

    public Dish() {
        this.dishId = 0;
        this.dishName = "";
        this.calories = 0;
        this.protein = 0;
        this.fat = 0;
        this.carbohydrate = 0;
        this.cookingTime = 0;
        this.ingredients = new ArrayList<>();
        this.tags = new HashSet<>();
        this.recipe = "";
        this.photo = "";
    }

    public long getDishId() {
        return dishId;
    }

    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public int getProtein() {
        return protein;
    }

    public void setProtein(int protein) {
        this.protein = protein;
    }

    public int getFat() {
        return fat;
    }

    public void setFat(int fat) {
        this.fat = fat;
    }

    public int getCarbohydrate() {
        return carbohydrate;
    }

    public void setCarbohydrate(int carbohydrate) {
        this.carbohydrate = carbohydrate;
    }

    public int getCookingTime() {
        return cookingTime;
    }

    public void setCookingTime(int cookingTime) {
        this.cookingTime = cookingTime;
    }

    public List<DishIngredients> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<DishIngredients> ingredients) {
        this.ingredients = ingredients;
    }

    public void addIngredients(DishIngredients ingredients) {
        this.ingredients.add(ingredients);
        this.calories = this.calories + ingredients.getCalories();
        this.protein = this.protein + ingredients.getProtein();
        this.fat = this.fat + ingredients.getFats();
        this.carbohydrate = this.carbohydrate + ingredients.getCarbohydrates();
    }

    public void deleteIngredients(DishIngredients ingredients) {
        this.ingredients.remove(ingredients);
        this.calories = this.calories <= ingredients.getCalories() ? 0 : this.calories - ingredients.getCalories();
        this.protein = this.protein <= ingredients.getProtein() ? 0 : this.protein - ingredients.getProtein();
        this.fat = this.fat <= ingredients.getFats() ? 0 : this.fat - ingredients.getFats();
        this.carbohydrate = this.carbohydrate <= ingredients.getCarbohydrates() ? 0 : this.carbohydrate - ingredients.getCarbohydrates();
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public String getTagsString() {
        StringBuffer stringBuffer = new StringBuffer();
        for(Tag tag : this.tags){
            stringBuffer.append(tag.getTag().trim());
            stringBuffer.append(" ");
        }
        return stringBuffer.substring(0);
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public void setTags(String tags) {
        try {
            tags = new String(tags.getBytes("UTF-8"));
            tags = tags.trim().toLowerCase().replace(","," ");
            String[] words = tags.split(" ");
            for(String word : words){
                if(word.length() > 0){
                    this.tags.add(new Tag(this,word));
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public String getRecipe() {
        return recipe.trim();
    }

    public void setRecipe(String recipe) {
        try {
            this.recipe =  new String(recipe.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dish dish = (Dish) o;
        return dishId == dish.dishId &&
                Objects.equals(dishName, dish.dishName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dishId, dishName);
    }
}