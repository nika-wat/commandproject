package ru.innopolis.stc20.diet.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * Класс работы с датами.
 * @autor Aleksey Danilchik
 */
public class DateUtils {

    /**
     * Метод возвращает количество полных лет с учётом текущей даты.
     * @param dateValue строковое представление даты формата <b>yyyy-MM-dd</b>.
     * @return years
     */
    public static long getNumberOfFullYears(String dateValue) {
        LocalDate birthDate = getLocalDate(dateValue);
        LocalDate now = LocalDate.now();
        long years = 0;
        if (birthDate != null)
            years = ChronoUnit.YEARS.between(birthDate, now);
        return years;
    }

    private static LocalDate getLocalDate(String dateValue) {
        LocalDate date = null;
        if (dateValue != null)
            date = LocalDate.parse(dateValue, DateTimeFormatter.ISO_LOCAL_DATE);
        return date;
    }
}
