package ru.innopolis.stc20.diet.utils;

import org.springframework.stereotype.Component;

import javax.ejb.EJB;
import javax.servlet.ServletContextEvent;
import java.io.IOException;
import java.util.*;

/**
 * Получаем все параметры из вложенных properties в web.xml
 * ключевой параметр all_param_context
 * @author Oleg_Chapurin
 */
@Component
public class ListPropertiesOfWebXml implements ListProperties {
    private static ListPropertiesOfWebXml propertiesTOfWebXml;
    private static Map<String, String> mapProperties = new HashMap<>();

    private ListPropertiesOfWebXml() {}

    private Iterator<String> getIteratorProperties(ServletContextEvent servletContextEvent, Properties properties,
                                                   String paramName) {
        Iterator<String> iteratorProperties = null;
        try {
            String param = servletContextEvent.getServletContext().getInitParameter(paramName);
            properties.load(servletContextEvent.getServletContext().getResourceAsStream(param));
            Set<String> setProperties = properties.stringPropertyNames();
            iteratorProperties = setProperties.iterator();
        } catch (IOException e) {
            // В дальнейшем заменить на функционал логирования
            e.printStackTrace();
        }
        return iteratorProperties;
    }

    public void fillProperties(ServletContextEvent servletContextEvent) {
        Properties properties = new Properties();
        Iterator<String> iteratorProperties =
                getIteratorProperties(servletContextEvent, properties, "all_param_context");
        while (iteratorProperties.hasNext()) {
            properties.clear();
            Iterator<String> iteratorProperties1 =
                    getIteratorProperties(servletContextEvent, properties, iteratorProperties.next());
            while (iteratorProperties1.hasNext()) {
                String key = iteratorProperties1.next();
                this.mapProperties.put(key, properties.getProperty(key));
            }
        }
    }

    public static ListProperties getInstance(ServletContextEvent servletContextEvent) {
        if (propertiesTOfWebXml == null) {
            synchronized (servletContextEvent){
                if (propertiesTOfWebXml == null) {
                    propertiesTOfWebXml = new ListPropertiesOfWebXml();
                    propertiesTOfWebXml.fillProperties(servletContextEvent);
                }
            }
        }
        return propertiesTOfWebXml;
    }

    /**
     * @param key - String ключ поиска
     * @return String результат поиска по ключу
     */
    @Override
    public String getProperty(String key) {
        return mapProperties.get(key);
    }

    /**
     * @param key   String ключ
     * @param value String значение
     */
    @Override
    public void put(String key, String value) {
        mapProperties.put(key,value);
    }
}
