package ru.innopolis.stc20.diet.utils;

import ru.innopolis.stc20.diet.entity.User;
import ru.innopolis.stc20.diet.enums.Gender;
import ru.innopolis.stc20.diet.enums.Purpose;

/**
 * Класс расчёта калорий.
 * @autor Aleksey Danilchik
 */
public class DailyCalories {
    private static final double COEFFICIENT_FOR_WEIGHT = 9.99;
    private static final double COEFFICIENT_FOR_GROWTH = 6.25;
    private static final double COEFFICIENT_FOR_YEARS = 4.92;
    private static final int COEFFICIENT_FOR_MAN = 5;
    private static final int COEFFICIENT_FOR_WOMAN = 161;

    private DailyCalories() {}

    public static int calculateNormCalories(User user) {
        int normCalories = 0;
        if (checkParametersNotNull(user) && checkParametersNotByDefault(user)) {
            if (user.getGender() == Gender.MALE) {
                normCalories = (int) ((basalMetabolicRate(user)
                                        + COEFFICIENT_FOR_MAN)
                                        * user.getActivity().getValue());
            } else {
                normCalories = (int) ((basalMetabolicRate(user)
                                        - COEFFICIENT_FOR_WOMAN)
                                        * user.getActivity().getValue());
            }

            int temp;
            if (user.getPurpose() == Purpose.SLIM) {
                temp = (int) (normCalories * Purpose.SLIM.getValue());
                normCalories -= temp;
            } else if (user.getPurpose() == Purpose.FAT) {
                temp = (int) (normCalories * Purpose.FAT.getValue());
                normCalories += temp;
            }
        }
        return normCalories;
    }

    private static boolean checkParametersNotNull(User user) {
        return user.getGender() != null
                && user.getBirthDate() != null
                && user.getPurpose() != null
                && user.getActivity() != null;
    }

    private static boolean checkParametersNotByDefault(User user) {
        return user.getHeight() > 0
                && user.getWeight() > 0;
    }

    private static int basalMetabolicRate(User user) {
        long year = DateUtils.getNumberOfFullYears(user.getBirthDate());
        return (int) ((COEFFICIENT_FOR_WEIGHT * user.getWeight())
                        + (COEFFICIENT_FOR_GROWTH * user.getHeight())
                        - (COEFFICIENT_FOR_YEARS * year));
    }
}
