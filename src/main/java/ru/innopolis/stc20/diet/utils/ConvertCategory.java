package ru.innopolis.stc20.diet.utils;

import ru.innopolis.stc20.diet.enums.Category;
import javax.persistence.AttributeConverter;

/**
 * @author Oleg_Chapurin
 */
public class ConvertCategory implements AttributeConverter<Category,String> {


    @Override
    public String convertToDatabaseColumn(Category category) {
        return category.getValue();
    }

    @Override
    public Category convertToEntityAttribute(String category) {
        return Category.getByValue(category);
    }
}
