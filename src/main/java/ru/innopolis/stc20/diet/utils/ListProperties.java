package ru.innopolis.stc20.diet.utils;

/**
 * Получаем все параметры из вложенных properties в web.xml
 * ключевой параметр all_param_context
 * @author Oleg_Chapurin
 */
public interface ListProperties {
    /**
     *
     * @param key - String ключ поиска
     * @return String результат поиска по ключу
     */
    String getProperty(String key);

    /**
     *
     * @param key String ключ
     * @param value String значение
     */
    void put(String key, String value);
}
