package ru.innopolis.stc20.diet.services;

import org.springframework.security.core.userdetails.UserDetailsService;
import ru.innopolis.stc20.diet.entity.User;
import ru.innopolis.stc20.diet.exceptions.NotEqualsPasswordException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

public interface UserService {
    /**
     * Метод сохраняет новый пароль пользователя.
     * @param newPassword новый пароль.
     * @param oldPassword старый пароль.
     * @return объект типа {@link User}.
     */
    User getUserWithChangePassword(String newPassword, String oldPassword) throws NotEqualsPasswordException;

    /**
     * Метод обновляет данные пользователя.
     * @param gender значение {@link ru.innopolis.stc20.diet.enums.Gender}.
     * @param purpose значение {@link ru.innopolis.stc20.diet.enums.Purpose}.
     * @param activity значение {@link ru.innopolis.stc20.diet.enums.Activity}.
     * @param date дата рождения пользователя.
     * @param height рост пользователя.
     * @param weight вес пользователя.
     * @param fullName ФИО пользователя.
     * @return объект типа {@link User}.
     */
    User updateUserFields(String gender, String purpose, String activity,
                          String date, int height, double weight, String fullName,
                          HttpServletRequest httpServletRequest, Part photo);

    /**
     * Метод возвращает объект типа {@link User}.
     * @return объект типа {@link User}.
     */
    User getUser();

    /**
     * Метод возвращает объект типа {@link User} по заданному {@code username}.
     * @param username логин пользователя.
     * @return объект типа {@link User}.
     */
    User getUserUsername(String username);

    /**
     * Метод сохраняет пользователя в БД.
     * @param user объект типа {@link User}.
     */
    void saveUser(User user);

    /**
     * Метод получает количество полных лет пользователя.
     * @return количество полных лет типа {@code long}.
     */
    long getNumberOfFullYearsUser();
}
