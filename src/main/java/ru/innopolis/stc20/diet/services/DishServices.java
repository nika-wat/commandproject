package ru.innopolis.stc20.diet.services;


import org.springframework.data.domain.Page;
import org.springframework.ui.Model;
import org.springframework.data.domain.Page;
import ru.innopolis.stc20.diet.entity.Dish;
import ru.innopolis.stc20.diet.entity.Ingredient;

import javax.servlet.http.Part;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * Сервис редактирования блюда
 * @author Oleg_Chapurin
 */
public interface DishServices {

    /**
     * Находим блюдо по id или создаем новое
     * @param idDish - id dish
     * @return object Dish
     */
    Dish getDishById(Long idDish);

    /**
     * Создаем новое блюдо заглушку
     * @return object Dish
     */
    Dish getDish();

    /**
     * Записываем блюдо в БД
     * @param dish - Блюдо
     * @param dishName - наименование блюда
     * @param cookingTime - время приготовления блюда
     * @param recipe - рецепт блюда
     * @param tedsSearch - теги поиска блюда
     * @param dataFile - файл фото
     * @return object Dish
     */
    Dish burnDish(Dish dish, String dishName, Integer cookingTime, String recipe, String tedsSearch,
                  Part dataFile, URL rootPath);

    /**
     * Удоляем блюдо из БД
     * @param idDish - id блюдо
     */
    void removeDish(Long idDish);

    /**
     * Добовляем ингредиент в блюдо
     * @param idIngredient - id ингредиента
     * @param weightGrams - вес ингредиент в граммах
     * @param idDish - id редактируемого блюда
     */
    Dish addIngredientToDish(Long idIngredient, Integer weightGrams, Long idDish);

    /**
     * Удоляем ингредиент из блюда
     * @param idDish - id редактируемое блюда удоляемого блюда
     * @param id_ingredient - id удоляемого ингредиента
     * @param weight - вес в граммах
     */
    Dish removeIngredientFromDish(long idDish,long id_ingredient,int weight);

    List<Dish> getAllDishes();
    Page<Dish> getDishesPage(Integer page, Integer size);
    List<Dish> getDishesWithParams(String[] dishNames, String tags, String calFrom, String calTo,
                                   String timeFrom, String timeTo, int limit, int offset);
    int getRowCount(String[] dishNames, String tags, String calFrom, String calTo,
                    String timeFrom, String timeTo);
    List<Ingredient> chooseIngredientsByCategory(String category);
    Map<String, Integer> getIndicatorsPer100Grams(Long id);
}
