package ru.innopolis.stc20.diet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.innopolis.stc20.diet.entity.Ingredient;
import ru.innopolis.stc20.diet.enums.Category;
import ru.innopolis.stc20.diet.repository.IngredientRepository;
import ru.innopolis.stc20.diet.specifications.IngredientPageFilter;

@Service
public class IngredientServicesImpl implements IngredientServices {
    private IngredientRepository ingredientRepository;

    @Autowired
    public IngredientServicesImpl(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    @Override
    public Page<Ingredient> getAllIngredientsByFilter(int page, int size, String descriptionText, String category,
                                                      int caloriesMin, int caloriesMax) {
        IngredientPageFilter ingredientPageFilter = new IngredientPageFilter(descriptionText,category,
                                                                                  caloriesMin,caloriesMax);
        Pageable pageable = PageRequest.of(page, size);
        Page<Ingredient> ingredientPage = ingredientRepository.findAll(ingredientPageFilter,pageable);
        return ingredientPage;
    }

    @Override
    public Page<Ingredient> getAllIngredients(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Ingredient> ingredientPage = ingredientRepository.findAllBy(pageable);
        return ingredientPage;
    }

    @Override
    public Ingredient createIngredient(String title, String category, Integer calories, Integer protein, Integer fats,
                                      Integer carbohydrates) {
        Ingredient ingredient = new Ingredient();
        ingredient.setTitle(title);
        ingredient.setCategory(Category.valueOf(category));
        ingredient.setCalories(calories);
        ingredient.setProtein(protein);
        ingredient.setFats(fats);
        ingredient.setCarbohydrates(carbohydrates);
        return ingredient;
    }

    @Override
    public void deleteIngredient(Long id) {
        ingredientRepository.deleteById(id);
    }

    @Override
    public Long saveIngredient(Ingredient ingredient) {
        return ingredientRepository.save(ingredient).getIngredientId();
    }

    @Override
    public Ingredient findByIngredientId(Long idIngredient) {
        return ingredientRepository.findByIngredientId(idIngredient);
    }
}
