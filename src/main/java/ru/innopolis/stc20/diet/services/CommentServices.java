package ru.innopolis.stc20.diet.services;

import org.springframework.data.domain.Page;
import ru.innopolis.stc20.diet.entity.Comment;
import ru.innopolis.stc20.diet.entity.Dish;
import ru.innopolis.stc20.diet.entity.User;

/**
 * @author Oleg_Chapurin
 */
public interface CommentServices {

    /**
     * Pagination
     * @param id_subj - id dish
     * @param page - number page
     * @param size - quantity comments on page
     * @return
     */
    Page<Comment> getDataForCommentPage(Long id_subj, Integer page, Integer size);

    /**
     * Save comment
     * @param text - text comment
     * @param user - author comment
     * @param dish - dish
     */
    void saveComment(String text, User user, Dish dish);
}
