package ru.innopolis.stc20.diet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.innopolis.stc20.diet.entity.Dish;
import ru.innopolis.stc20.diet.entity.DishIngredients;
import ru.innopolis.stc20.diet.entity.Ingredient;
import ru.innopolis.stc20.diet.entity.Tag;
import ru.innopolis.stc20.diet.enums.Category;
import ru.innopolis.stc20.diet.repository.DishIngredientRepository;
import ru.innopolis.stc20.diet.repository.DishRepository;
import ru.innopolis.stc20.diet.repository.IngredientRepository;
import ru.innopolis.stc20.diet.repository.TagRepository;

import javax.servlet.http.Part;
import java.net.URL;
import java.util.*;

/**
 * Сервис редактирования блюда
 * @author Oleg_Chapurin
 */
@Service
public class DishServicesImpl implements DishServices {
    private DishRepository dishRepository;
    private DishIngredientRepository dishIngredientRepository;
    private TagRepository tagRepository;
    private IngredientRepository ingredientRepository;
    private FileProcessingServices fileProcessingServices;
    private TagService tagService;

    @Autowired
    public DishServicesImpl(DishRepository dishRepository, DishIngredientRepository dishIngredientRepository,
                            TagRepository tagRepository,IngredientRepository ingredientRepository,
                            FileProcessingServices fileProcessingServices,TagService tagService) {
        this.dishRepository = dishRepository;
        this.dishIngredientRepository = dishIngredientRepository;
        this.tagRepository = tagRepository;
        this.ingredientRepository = ingredientRepository;
        this.fileProcessingServices = fileProcessingServices;
        this.tagService = tagService;
    }

    /**
     * Находим блюдо по id или создаем новое
     * @param idDish - id dish
     * @return object Dish
     */
    public Dish getDishById(Long idDish) {
        Dish dish;
        if (idDish != 0L) {
            dish = dishRepository.findByDishId(idDish);
        }else {
            dish = getDish();
        }
        return dish;
    }

    /**
     * Создаем новое блюдо заглушку
     *
     * @return object Dish
     */
    @Override
    public Dish getDish() {
        Dish dish = new Dish();
        dish.setDishId(0);
        return dish;
    }

    /**
     * Записываем блюдо в БД
     * @param dish - Блюдо
     * @param dishName - наименование блюда
     * @param cookingTime - время приготовления блюда
     * @param recipe - рецепт блюда
     * @param tedsSearch - теги поиска блюда
     * @param dataFile - файл фото
     */
    @Override
    public Dish burnDish(Dish dish, String dishName, Integer cookingTime, String recipe, String tedsSearch,
                         Part dataFile, URL rootPath) {
        dish.setDishName(dishName);
        dish.setCookingTime(cookingTime);
        dish.setRecipe(recipe);
        dish.setPhoto(fileProcessingServices.writeDataUploadFileAndGetPath(dataFile, rootPath.getPath()));
        dish = dishRepository.save(dish);
        tagService.writeTags(dish,tedsSearch);
        return dish;
    }

    /**
     * Удоляем блюдо из БД
     * @param idDish - id dish
     */
    @Override
    public void removeDish(Long idDish) {
        Dish dish = dishRepository.findByDishId(idDish);
        List<DishIngredients> dishIngredientsList = dish.getIngredients();
        for (DishIngredients dishIngredients:dishIngredientsList) {
            dishIngredientRepository.delete(dishIngredients);
        }
        Set<Tag> tagList = dish.getTags();
        for (Tag tag:tagList) {
            tagRepository.delete(tag);
        }
        dishRepository.delete(dish);
    }

    /***
     * Добовляем ингредиент в блюдо
     * @param idIngredient - id ингредиента
     * @param weight - weight in gr.
     * @param idDish - id dish
     * @return
     */
    @Override
    public Dish addIngredientToDish(Long idIngredient, Integer weight, Long idDish) {
        Dish dish;
        Ingredient ingredient = ingredientRepository.findByIngredientId(idIngredient);
        dish = dishRepository.findByDishId(idDish);
        if ((ingredient != null) && (dish != null)) {
            DishIngredients dishIngredients = new DishIngredients(dish, ingredient, weight);
            dishIngredientRepository.save(dishIngredients);
            dish.addIngredients(dishIngredients);
            dishRepository.save(dish);
        }
        dish = dishRepository.findByDishId(idDish);
        return dish;
    }

    /**
     * Удоляем ингредиент из блюда
     * @param idDish - id dish
     * @param id_ingredient - id ingredient
     * @param weight - weight
     * @return - object class Dish
     */
    @Override
    public Dish removeIngredientFromDish(long idDish,long id_ingredient,int weight) {
        DishIngredients dishIngredients = dishIngredientRepository.findDishIngredientsByDish_DishIdAndIngredient_IngredientId(idDish,
                                                                                                   id_ingredient);
         if(dishIngredients != null) {
             if ((dishIngredients.getWeight() == weight) || (dishIngredients.getWeight() < weight)) {
                 dishIngredientRepository.delete(dishIngredients);
             }
             if (dishIngredients.getWeight() > weight) {
                 dishIngredients.setWeight(dishIngredients.getWeight() - weight);
                 dishIngredientRepository.save(dishIngredients);
             }
         }
        Dish dish = dishRepository.findByDishId(idDish);
       return dish;
    }

    /**
     * Выбираем ингредиенты по категории
     * @param category - категория
     * @return - List ingredients
     */
    @Override
    public List<Ingredient> chooseIngredientsByCategory(String category) {
        List<Ingredient> listIngredients = new ArrayList<>();
        if (category.length() > 0) {
            listIngredients = ingredientRepository.findAllByCategory(Category.valueOf(category));
        }
        return listIngredients;
    }

    @Override
    public List<Dish> getAllDishes() {
        return dishRepository.findAll();
    }

    @Override
    public Page<Dish> getDishesPage(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("dishName").ascending());
        return dishRepository.findAll(pageable);
    }

    @Override
    public List<Dish> getDishesWithParams(String[] dishNames, String tags, String calFrom, String calTo,
                                          String timeFrom, String timeTo, int limit, int offset) {
        return dishRepository.getAllDishesWithParams(dishNames, tags, calFrom, calTo,
                timeFrom, timeTo, limit, offset);
    }

    @Override
    public int getRowCount(String[] dishNames, String tags, String calFrom, String calTo,
                           String timeFrom, String timeTo) {
        List<Dish> dishList = dishRepository.getAllDishesWithParams(dishNames, tags, calFrom, calTo,
                timeFrom, timeTo, 0, 0);
        return dishList.size();
    }

    @Override
    public Map<String, Integer> getIndicatorsPer100Grams(Long id) {
        Dish dish = dishRepository.getOne(id);
        int weight = dishIngredientRepository.findAllByWeight(id).orElse(0);
        Map<String, Integer> map = new HashMap<>();
        map.put("calories100", getCalculationOfIndicators(dish.getCalories(), weight));
        map.put("protein100", getCalculationOfIndicators(dish.getProtein(), weight));
        map.put("fat100", getCalculationOfIndicators(dish.getFat(), weight));
        map.put("carbohydrate100", getCalculationOfIndicators(dish.getCarbohydrate(), weight));
        return map;
    }

    private int getCalculationOfIndicators(int indicators, int weight) {
        return  ((indicators > 0) && (weight > 0)) ? (indicators * 100 / weight) : 0;
    }
}
