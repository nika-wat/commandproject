package ru.innopolis.stc20.diet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.stc20.diet.entity.Dish;
import ru.innopolis.stc20.diet.entity.Tag;
import ru.innopolis.stc20.diet.repository.TagRepository;

import java.io.UnsupportedEncodingException;

/**
 * @author Oleg_Chapurin
 */
@Service
public class TagServiceImpl implements TagService {
    private TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository){
        this.tagRepository = tagRepository;
    }

    @Override
    public void writeTags(Dish dish, String tags) {
        try {
            tags = new String(tags.getBytes("UTF-8"));
            tags = tags.trim().toLowerCase().replace(","," ");
            String[] words = tags.split(" ");
            for(String word : words){
                if(word.length() > 0){
                    tagRepository.save(new Tag(dish,word));
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeTags(Tag tag) {
        tagRepository.delete(tag);
    }
}
