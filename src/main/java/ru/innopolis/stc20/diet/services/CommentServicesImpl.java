package ru.innopolis.stc20.diet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.innopolis.stc20.diet.entity.Comment;
import ru.innopolis.stc20.diet.entity.Dish;
import ru.innopolis.stc20.diet.entity.User;
import ru.innopolis.stc20.diet.enums.TypeSubj;
import ru.innopolis.stc20.diet.repository.CommentRepository;
import ru.innopolis.stc20.diet.repository.DishRepository;

import java.time.OffsetDateTime;
import java.time.ZoneId;

/**
 * @author Oleg_Chapurin
 */
@Service
public class CommentServicesImpl implements CommentServices {
    private CommentRepository commentRepository;
    private DishRepository dishRepository;

    @Autowired
    public CommentServicesImpl(CommentRepository commentRepository, DishRepository dishRepository) {
        this.commentRepository = commentRepository;
        this.dishRepository = dishRepository;
    }

    /**
     * Pagination
     * @param id_subj - id dish
     * @param page - number page
     * @param size - quantity comments on page
     * @return
     */
    @Override
    public Page<Comment> getDataForCommentPage(Long id_subj, Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Comment> commentPage = commentRepository.findByIdSubjDiscussionsOrderByCreationDateDesc(id_subj, pageable);
        return commentPage;
    }

    /**
     * Save comment
     * @param text - text comment
     * @param user - author comment
     * @param dish - dish
     */
    @Override
    public void saveComment(String text, User user, Dish dish) {
        if ((dish != null) && (user != null) && (text.length() > 0)) {
            Comment comment = new Comment(user, text, OffsetDateTime.now(ZoneId.systemDefault()),
                    dish.getDishId(), TypeSubj.DISH);
            commentRepository.save(comment);
        }
    }
}
