package ru.innopolis.stc20.diet.services;

import ru.innopolis.stc20.diet.entity.Dish;
import ru.innopolis.stc20.diet.entity.Ingredient;
import ru.innopolis.stc20.diet.entity.User;
import javax.servlet.http.Part;

/**
 * @author Oleg_Chapurin
 */
public interface FileProcessingServices {

    /**
     *
     * @param filePart - фрагмент HTTP с типом multipart/form-data
     * @param dish - объект класса Dish
     * @param rootPath - путь до корневой папки с рассположением графических файлов
     */
    void writeDataUploadFileForDish(Part filePart, Dish dish, String rootPath);

    /**
     *
     * @param filePart - фрагмент HTTP с типом multipart/form-data
     * @param ingredient - объект класса Ingredient
     * @param rootPath - путь до корневой папки с рассположением графических файлов
     */
    void writeDataUploadFileForIngredients(Part filePart, Ingredient ingredient, String rootPath);

    /**
     *
     * @param filePart - фрагмент HTTP с типом multipart/form-data
     * @param user - объект класса User
     */
    void writeDataUploadFileForUsers(Part filePart, User user, String rootPath);

    String writeDataUploadFileAndGetPath(Part filePart, String rootPath);
}
