package ru.innopolis.stc20.diet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.innopolis.stc20.diet.entity.User;
import ru.innopolis.stc20.diet.enums.Activity;
import ru.innopolis.stc20.diet.enums.Gender;
import ru.innopolis.stc20.diet.enums.Purpose;
import ru.innopolis.stc20.diet.exceptions.NotEqualsPasswordException;
import ru.innopolis.stc20.diet.repository.UserRepository;
import ru.innopolis.stc20.diet.utils.DailyCalories;
import ru.innopolis.stc20.diet.utils.DateUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.net.MalformedURLException;
import java.net.URL;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private FileProcessingServices fileProcessingServices;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, FileProcessingServices fileProcessingServices) {
        this.userRepository = userRepository;
        this.fileProcessingServices = fileProcessingServices;
    }

    @Bean
    public PasswordEncoder bcryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public User getUserWithChangePassword(String newPassword, String oldPassword)
            throws NotEqualsPasswordException {
        User user = getUser();
        if (newPassword != null && oldPassword != null) {
            String oldPasswordDb = user.getPassword();
            if (bcryptPasswordEncoder().matches(oldPassword, oldPasswordDb)) {
                user.setPassword(bcryptPasswordEncoder().encode(newPassword));
                userRepository.save(user);
            } else {
                throw new NotEqualsPasswordException("Старый пароль неверен.");
            }
        }
        return getUser();
    }

    @Override
    public User updateUserFields(String gender, String purpose, String activity,
                                 String date, int height, double weight, String fullName,
                                 HttpServletRequest httpServletRequest, Part photo) {
        User user = getUser();
        user.setGender(Gender.valueOf(gender));
        user.setPurpose(Purpose.valueOf(purpose));
        user.setActivity(Activity.valueOf(activity));
        user.setBirthDate(date);
        user.setHeight(height);
        user.setWeight(weight);
        user.setFullName(fullName);
        user.setNormCalories(DailyCalories.calculateNormCalories(user));

        try {
            URL rootPath = httpServletRequest.getServletContext().getResource("/");
            user.setPhoto(fileProcessingServices.writeDataUploadFileAndGetPath(photo, rootPath.getPath()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        userRepository.save(user);
        return getUser();
    }

    @Override
    public long getNumberOfFullYearsUser() {
        User user = getUser();
        return DateUtils.getNumberOfFullYears(user.getBirthDate());
    }

    @Override
    public User getUser() {
        return userRepository.findByUsername(getUserNameFromSecurityContext());
    }

    private String getUserNameFromSecurityContext() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return ((UserDetails)principal).getUsername();
        } else {
            return principal.toString();
        }
    }

    @Override
    public User getUserUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }
}
