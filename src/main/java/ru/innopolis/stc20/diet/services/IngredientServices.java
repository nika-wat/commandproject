package ru.innopolis.stc20.diet.services;

import org.springframework.data.domain.Page;
import ru.innopolis.stc20.diet.entity.Ingredient;

public interface IngredientServices {
    Page<Ingredient>  getAllIngredientsByFilter(int page, int size, String descriptionText, String category,
                                                int caloriesMin, int caloriesMax);
    Page<Ingredient> getAllIngredients(int page, int size);
    Ingredient createIngredient(String title, String category, Integer calories, Integer protein, Integer fats,
                                Integer carbohydrates);
    void deleteIngredient(Long id);

    Long saveIngredient(Ingredient ingredient);

    Ingredient findByIngredientId(Long idIngredient);
}
