package ru.innopolis.stc20.diet.services;

import ru.innopolis.stc20.diet.entity.Dish;
import ru.innopolis.stc20.diet.entity.Tag;

/**
 * @author Oleg_Chapurin
 */
public interface TagService {

    void writeTags(Dish dish, String tags);
    void removeTags(Tag tag);
}
