package ru.innopolis.stc20.diet.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.innopolis.stc20.diet.entity.Dish;
import ru.innopolis.stc20.diet.entity.Ingredient;
import ru.innopolis.stc20.diet.entity.User;
import ru.innopolis.stc20.diet.utils.ListProperties;
import javax.servlet.http.Part;
import java.io.*;
import java.nio.file.Paths;

/**
 * @author Oleg_Chapurin
 */
@Service
public class FileProcessingServicesImpl implements FileProcessingServices {

    private static int DEFAULT_BUFFER_SIZE = 1024;
    private String pathFileImg;

    @Autowired
    public FileProcessingServicesImpl(@Value("${file.pathFileImg}") String pathFileImg) {
        this.pathFileImg = pathFileImg;
    }

    private String setEncodingString(String str, String encoding, String systemEncoding) {
        try {
            str = new String(str.getBytes(encoding), systemEncoding);
        } catch (UnsupportedEncodingException e) {
            //Добавить логирование
            e.printStackTrace();
        }
        return str;
    }

    private String uploadFile(String rootPath,Part filePart) {
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        String pageFileName = pathFileImg.concat(fileName).replace('\\', '/');
        rootPath = rootPath.concat(pathFileImg);
        rootPath = rootPath.replace('\\', '/');
        String fullFileName = setEncodingString(rootPath.concat(fileName),
                "UTF-8", System.getProperty("file.encoding"));
        try (BufferedInputStream bufferedInputStream =
                     new BufferedInputStream(filePart.getInputStream(), DEFAULT_BUFFER_SIZE);
             BufferedOutputStream bufferedOutputStream =
                     new BufferedOutputStream(new FileOutputStream(fullFileName), DEFAULT_BUFFER_SIZE)) {
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            while ((bufferedInputStream.read(buffer) > 0)) {
                bufferedOutputStream.write(buffer);
            }
            return pageFileName;
        } catch (IOException e) {
            //Добавить логирование
            e.printStackTrace();
        }
        return "";
    }

    private void uploadFileDish(String rootPath,Part filePart, Dish dish){
        dish.setPhoto(uploadFile(rootPath,filePart));
    }

    private void uploadFileIngredient(String rootPath,Part filePart,Ingredient ingredient){
        ingredient.setPhoto(uploadFile(rootPath,filePart));
    }

    private void uploadFileUser(Part filePart, User user, String rootPath){
        user.setPhoto(uploadFile(rootPath, filePart));
    }

    /**
     *
     * @param filePart - фрагмент HTTP с типом multipart/form-data
     * @param dish - объект класса Dish
     * @param rootPath - путь до корневой папки с рассположением графических файлов
     */
    @Override
    @Deprecated
    public void writeDataUploadFileForDish(Part filePart, Dish dish, String rootPath) {
        if ((filePart != null) && (filePart.getSize() > 0)) {
            uploadFileDish(rootPath,filePart, dish);
        }
    }

    /**
     *
     * @param filePart - фрагмент HTTP с типом multipart/form-data
     * @param ingredient - объект класса Ingredient
     * @param rootPath - путь до корневой папки с рассположением графических файлов
     */
    @Override
    @Deprecated
    public void writeDataUploadFileForIngredients(Part filePart, Ingredient ingredient, String rootPath) {
        if ((filePart != null) && (filePart.getSize() > 0)) {
            uploadFileIngredient(rootPath,filePart, ingredient);
        }
    }

    /**
     *
     * @param filePart - фрагмент HTTP с типом multipart/form-data
     * @param user - объект класса User
     */
    @Override
    @Deprecated
    public void writeDataUploadFileForUsers(Part filePart, User user, String rootPath) {
        if ((filePart != null) && (filePart.getSize() > 0)) {
            uploadFileUser(filePart, user, rootPath);
        }
    }

    @Override
    public String writeDataUploadFileAndGetPath(Part filePart, String rootPath) {
        if ((filePart != null) && (filePart.getSize() > 0)) {
            return uploadFile(rootPath, filePart);
        }
        return null;
    }
}
