package ru.innopolis.stc20.diet.specifications;

import org.springframework.data.jpa.domain.Specification;
import ru.innopolis.stc20.diet.entity.Ingredient;
import ru.innopolis.stc20.diet.enums.Category;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Oleg_Chapurin
 */
public class IngredientPageFilter implements Specification<Ingredient> {
    @NotNull
    private String descriptionText;
    @NotNull
    private String category;
    private int caloriesMin;
    private int caloriesMax;

    public IngredientPageFilter(String descriptionText, String category, int caloriesMin, int caloriesMax) {
        this.descriptionText = descriptionText;
        this.category = category;
        this.caloriesMin = caloriesMin;
        this.caloriesMax = caloriesMax;
    }

    @Override
    public Predicate toPredicate(Root<Ingredient> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        final List<Predicate> predicates = new ArrayList<Predicate>();
        if(! "".equals(descriptionText)){
            predicates.add(criteriaBuilder.like(root.get("title"),descriptionText));
        }
        if(! "".equals(category)){
            String categoryFilter = Category.valueOf(category).getValue();
            predicates.add(criteriaBuilder.equal(root.get("category"),Category.valueOf(category)));//root.get("category").alias(categoryFilter).getAlias()
        }
        if(0 < caloriesMin){
            predicates.add(criteriaBuilder.ge(root.get("calories"),caloriesMin));
        }
        if(0 < caloriesMax){
            predicates.add(criteriaBuilder.le(root.get("calories"),caloriesMax));
        }
        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
