package ru.innopolis.stc20.diet.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.innopolis.stc20.diet.entity.Comment;


/**
 * @author Oleg_Chapurin
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment,Long> {
    Page<Comment> findByIdSubjDiscussionsOrderByCreationDateDesc(Long id_subj, Pageable pageable);
}
