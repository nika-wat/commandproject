package ru.innopolis.stc20.diet.repository;

import ru.innopolis.stc20.diet.entity.Dish;

import java.util.List;

public interface DishRepositoryCustom {
    List<Dish> getAllDishesWithParams(String[] dishNames, String tags, String calFrom, String calTo,
                                     String timeFrom, String timeTo, int limit, int offset);
}
