package ru.innopolis.stc20.diet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.innopolis.stc20.diet.entity.Dish;

/**
 * @author Oleg_Chapurin
 */
@Repository
public interface DishRepository  extends JpaRepository<Dish,Long>, DishRepositoryCustom {
    Dish findByDishId(Long idDish);
}
