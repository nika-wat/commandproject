package ru.innopolis.stc20.diet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.innopolis.stc20.diet.entity.DishIngredients;

import java.util.List;
import java.util.Optional;

/**
 * @author Oleg_Chapurin
 */
@Repository
public interface DishIngredientRepository extends JpaRepository<DishIngredients,Long> {
    DishIngredients findDishIngredientsByDish_DishIdAndIngredient_IngredientId(Long idDish,Long idIngredient);
    List<DishIngredients> findAllByDish(Long idDish);

    @Query(value = "SELECT SUM (weight) from dish_ingredients d where d.dish_id=:idDish", nativeQuery = true)
    Optional<Integer> findAllByWeight(Long idDish);
    /*List<DishIngredients> getDishIngredientsByIdDish(Dish dish);*/
}
