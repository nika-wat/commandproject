package ru.innopolis.stc20.diet.repository;

import ru.innopolis.stc20.diet.entity.Dish;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

public class DishRepositoryCustomImpl implements DishRepositoryCustom {
    private static final String SELECT_ALL_DISHES =
            "SELECT dish_id, calories, carbohydrate, cooking_time, dish_name, fat, photo, protein, recipe FROM DISH";
    private static final String SELECT_LIMIT = " LIMIT %d OFFSET %d";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Dish> getAllDishesWithParams(String[] dishNames, String tags, String calFrom, String calTo,
                                            String timeFrom, String timeTo, int limit, int offset) {
        String query = getQueryWithParams(SELECT_ALL_DISHES, dishNames, tags, calFrom, calTo, timeFrom, timeTo);

        if (limit > 0)
            query = query + String.format(SELECT_LIMIT, limit, offset);

        return entityManager.createNativeQuery(query, Dish.class).getResultList();
    }

    private String getQueryWithParams(String startQuery, String[] dishNames, String tags,
                                      String calFrom, String calTo,
                                      String timeFrom, String timeTo){
        List<String> condition = new ArrayList<>();

        if (dishNames != null) {
            String strDishNames = String.join("','", dishNames);
            condition.add(" DISH_NAME IN ('" + strDishNames + "')");
        }

        if (tags != null && !tags.isEmpty()) {
            String[] tagsArray;
            tagsArray = tags.split("\\s*,\\s*");
            String strTags = String.join("','", tagsArray);
            condition.add(" EXISTS(SELECT 1 FROM TAGS T WHERE T.DISH_ID = DISH.DISH_ID " +
                    "AND LOWER(TAG) IN ('" + strTags + "'))");
        }

        if (calFrom != null && !calFrom.isEmpty())
            condition.add(" CALORIES >= " + Integer.valueOf(calFrom));

        if (calTo != null && !calTo.isEmpty())
            condition.add(" CALORIES <= " + Integer.valueOf(calTo));

        if (timeFrom != null && !timeFrom.isEmpty())
            condition.add(" COOKING_TIME >= " + Integer.valueOf(timeFrom));

        if (timeTo != null && !timeTo.isEmpty())
            condition.add(" COOKING_TIME <= " + Integer.valueOf(timeTo));

        String query = startQuery;
        if (condition.size() > 0)
            query = query + " WHERE " + String.join(" AND ", condition);

        return query;
    }
}
