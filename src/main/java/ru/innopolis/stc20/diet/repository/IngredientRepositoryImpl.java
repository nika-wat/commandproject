package ru.innopolis.stc20.diet.repository;

import org.springframework.stereotype.Repository;
import ru.innopolis.stc20.diet.entity.Dish;
import ru.innopolis.stc20.diet.entity.DishIngredients;
import ru.innopolis.stc20.diet.entity.Ingredient;


import java.util.Collection;
import java.util.List;

/*@Repository*/
public class IngredientRepositoryImpl  {

    public void addIngredient(Ingredient ingredient) {

    }


    public void updateIngredient(Ingredient ingredient) {

    }


    public void deleteIngredient(Long ingredientId) {

    }


    public Collection<Ingredient> getAllIngredients() {
        return null;
    }


    public Collection<Ingredient> getAllIngredients(String descriptionText, String category, String caloriesMin, String caloriesMax, int linesPerPage, int offset) {
        return null;
    }


    public Collection<Ingredient> getSomeIngredients(int linesPerPage, int offset) {
        return null;
    }


    public Ingredient getOne(Long ingredientId) {
        return null;
    }


    public int getNumberOfRows() {
        return 0;
    }


    public List<DishIngredients> getDishIngredientsByIdDish(Dish dish) {
        return null;
    }


    public List<Ingredient> getIngredientsByCategory(String category) {
        return null;
    }


    public DishIngredients getDishIngredientsById(Dish dish, long id, int weightGrams) {
        return null;
    }
}
