package ru.innopolis.stc20.diet.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.innopolis.stc20.diet.entity.Ingredient;
import ru.innopolis.stc20.diet.enums.Category;

import java.util.List;

@Repository
public interface IngredientRepository  extends JpaRepository<Ingredient,Long> , JpaSpecificationExecutor<Ingredient> {
    Ingredient findByIngredientId(Long idIngredient);
    List<Ingredient> findAllByCategory(Category category);
    Page<Ingredient> findAllBy(Pageable pageable);
}
