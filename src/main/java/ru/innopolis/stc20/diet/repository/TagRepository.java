package ru.innopolis.stc20.diet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.innopolis.stc20.diet.entity.Dish;
import ru.innopolis.stc20.diet.entity.Tag;
import java.util.List;

/**
 * @author Oleg_Chapurin
 */
@Repository
public interface TagRepository extends JpaRepository<Tag,Long> {

    List<Tag> findByDish(Dish dish);
    /*List<String> getTagsByIdDish(long idDish);*/
}
