package ru.innopolis.stc20.diet.exceptions;

public class NotEqualsPasswordException extends Exception {
    public NotEqualsPasswordException(String message) {
        super(message);
    }
}
