<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Alex
  Date: 04.03.2020
  Time: 21:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<div class="centerDish" >
        <div class="container">
            <div class="form-row justify-content-center">
                <div class="col-12 input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Наименование блюда</span>
                    </div>
                    <input class="form-control" type="text" value="<c:out value="${dish.getDishName()}"/>"
                           name="dishName" readonly>
                </div>
            </div>

            <div class="form-row justify-content-center">
                <div class="col-3">
                    <div class="card" style="width: 17rem; height: 16rem">
                        <img src="<c:out value="${path_js_css}"/><c:out value="${dish.getPhoto()}"/>"
                             class="rounded float-left" alt="..." style="width: 17rem; height: 16rem" name="img">
                    </div>
                </div>
                <div class="col-9 mb-3">
                    <label for="exampleFormControlTextarea1">Рецепт</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="10" name="recipe" readonly>
                        <c:out value="${dish.getRecipe()}"/>
                    </textarea>
                </div>
            </div>

            <div class="form-row justify-content-center">
                <div class="col-12 input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Теги поиска</span>
                    </div>
                    <input class="form-control" type="text" value="<c:out value="${dish.getTagsString()}"/>"
                           name="tedsSearch" readonly>
                </div>
            </div>

            <div class="form-row justify-content-center">
                <div class="col-3 input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Калории</span>
                    </div>
                    <input class="form-control" type="text" step="00000.01" min="0" placeholder="00000,00"
                           id="id05" name="calories" disabled style="background-color:#FFFFFF" readonly
                           value="<c:out value="${dish.getCalories()}"/>">
                </div>
                <div class="col-3 input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Белки</span>
                    </div>
                    <input class="form-control" type="text" step="00000.01" min="0" placeholder="00000,00"
                           name="protein" disabled style="background-color:#FFFFFF" readonly
                           value="<c:out value="${dish.getProtein()}"/>">
                </div>
                <div class="col-3 input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Жиры</span>
                    </div>
                    <input class="form-control" type="text" step="00000.01" min="0" placeholder="00000,00"
                           name="fat" disabled style="background-color:#FFFFFF" readonly
                           value="<c:out value="${dish.getFat()}"/>">
                </div>
                <div class="col-3 input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Углеводы</span>
                    </div>
                    <input class="form-control" type="text" step="00000.01" min="0" placeholder="00000,00"
                           name="carbohydrate" disabled style="background-color:#FFFFFF" readonly
                           value="<c:out value="${dish.getCarbohydrate()}"/>">
                </div>
            </div>

            <div class="form-row justify-content-center">
                <div class="col-3 input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Калории (на 100 гр.)</span>
                    </div>
                    <input class="form-control" type="text" step="00000.01" min="0" placeholder="00000,00"
                           name="calories" disabled style="background-color:#FFFFFF" readonly
                           value="<c:out value="${map.get('calories100')}"/>">
                </div>
                <div class="col-3 input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Белки (на 100 гр.)</span>
                    </div>
                    <input class="form-control" type="text" step="00000.01" min="0" placeholder="00000,00"
                           name="protein" disabled style="background-color:#FFFFFF" readonly
                           value="<c:out value="${map.get('protein100')}"/>">
                </div>
                <div class="col-3 input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Жиры (на 100 гр.)</span>
                    </div>
                    <input class="form-control" type="text" step="00000.01" min="0" placeholder="00000,00"
                           name="fat" disabled style="background-color:#FFFFFF" readonly
                           value="<c:out value="${map.get('fat100')}"/>">
                </div>
                <div class="col-3 input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Углеводы (на 100 гр.)</span>
                    </div>
                    <input class="form-control" type="text" step="00000.01" min="0" placeholder="00000,00"
                           name="carbohydrate" disabled style="background-color:#FFFFFF" readonly
                           value="<c:out value="${map.get('carbohydrate100')}"/>">
                </div>
            </div>

            <div class="row justify-content-between">
                <div class="col-5 input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Время приготовления (мин.)</span>
                    </div>
                    <input class="form-control" type="text" step="00000" min="0" placeholder="00000"
                           value="<c:out value="${dish.getCookingTime()}"/>" name="cookingTime" readonly>
                </div>

                <form method="get" action="/dishes/<c:out value="${dish.getDishId()}"/>">
                    <div class="m-1" align="right">
                        <button type="submit" class="btn btn-primary" value="true">Редактировать</button>
                        <input type="text" hidden name="editable" value="true" />
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    </div>
                </form>

                <form method="post" action="/dishes/<c:out value="${dish.getDishId()}"/>/delete">
                    <div class="m-2" align="right">
                        <button type="submit" class="btn btn-primary" value="true">Удалить</button>
                    </div>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                </form>
            </div>
        </div>

</div>

    <div class="container">
        <br/>
    </div>

    <c:if test="${dish.getDishId() > 0}">
        <div class="centerDish ">
            <div class="container">
                <div class="row ">
                    <c:set var="ingredients" value="${dish.getIngredients()}"/>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Ингредиенты</th>
                                <th scope="col" style="width: 8rem;">Вес в гр.</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="ingredient" items="${ingredients}">
                                <tr>
                                    <td><c:out value="${ingredient.getIngredient().getTitle()}"/></td>
                                    <td><c:out value="${ingredient.getWeight()}"/></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </c:if>

<tiles:insertTemplate template="_comments.jsp"/>
