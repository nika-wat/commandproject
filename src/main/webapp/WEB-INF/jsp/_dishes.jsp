<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row col ml-3" >
    <h4>Список блюд</h4><br>
</div>
<div class="container row col-12 ml-3">
    <form method = "GET" action="${pageContext.request.contextPath}/dishes" autocomplete="off">
        <div class="form-group row">
            <label for="dishNames" class="col-3 col-form-label">Продукты:</label>
            <div class="col">
                <select multiple class="form-control" name="dishNames" size="2" id="dishNames">
                    <c:forEach var="dish" items="${allDishes}">
                        <option>${dish.dishName}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="tags" class="col-3 col-form-label">Ключевые поля:</label>
            <div class="col">
                <input type="text" class="form-control" id="tags" name="tags" value="${tags}" placeholder="пример: завтрак,фитнес,каша">
            </div>
        </div>
        <div class="form-group row">
            <label for="calFrom" class="col-3 col-form-label">Калорийность от</label>
            <div class="col">
                <input class="form-control" type="number" id="calFrom" name="calFrom" value="${calFrom}" placeholder="0">
            </div>
            <label for="calTo" class="col-1 col-form-label">до</label>
            <div class="col">
                <input class="form-control" type="number" id="calTo" name="calTo" value="${calTo}" placeholder="1000">
            </div>
        </div>
        <div class="form-group row">
            <label for="timeFrom" class="col-3 col-form-label">Время приготовления (мин.) от</label>
            <div class="col">
                <input class="form-control" type="number" id="timeFrom" name="timeFrom" value="${timeFrom}" placeholder="0">
            </div>
            <label for="timeTo" class="col-1 col-form-label">до</label>
            <div class="col">
                <input class="form-control" type="number" id="timeTo" name="timeTo" value="${timeTo}" placeholder="180">
            </div>
        </div>
        <button type="submit" class="btn btn-primary my-1">Найти</button>
    </form>
</div>

<c:if test="${authorized == true}">
    <div class="row col-sm-auto ml-3">
        <form method="get" action="/dishes/add">
            <button type="submit" class="btn btn-primary my-1">Создать блюдо</button>
        </form>
    </div>
</c:if>

<div class="row col-sm-auto ml-3">
    <table class="table table-hover table-bordered">
        <thead>
        <tr>
            <th scope="col">ИД</th>
            <th scope="col">Наименование</th>
            <th scope="col">Калорийность</th>
            <th scope="col">Белки</th>
            <th scope="col">Жиры</th>
            <th scope="col">Углеводы</th>
            <th scope="col">Время приготовления</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="dish" items="${dishes}">
            <tr>
                <td scope="row">${dish.dishId}</td>
                <td>${dish.dishName}</td>
                <td>${dish.calories}</td>
                <td>${dish.protein}</td>
                <td>${dish.fat}</td>
                <td>${dish.carbohydrate}</td>
                <td>${dish.cookingTime}</td>
                <td>
                    <form method="get" action="${pageContext.request.contextPath}/dishes/<c:out value="${dish.dishId}"/>" style="display:inline">
                        <input type="text" hidden name="editable" value="false" />
                        <input type="submit" class="btn btn-outline-warning btn-sm" value="Детально" style="width: 7rem">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    </form>
                    <br><br>
                    <form method="get" action="${pageContext.request.contextPath}/comments/<c:out value="${dish.dishId}"/>/0" style="display:inline">
                        <input type="text" hidden name="editable" value="false" />
                        <input type="submit" class="btn btn-outline-warning btn-sm" value="Комментарии" style="width: 7rem">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    </form>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <nav aria-label="Navigation for dishes">
        <ul class="pagination">
            <c:if test="${currentPage != 1}">
                <li class="page-item"><a class="page-link"
                                         href="dishes?currentPage=${currentPage-1}">Назад</a>
                </li>
            </c:if>

            <c:forEach begin="1" end="${noOfPages}" var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <li class="page-item active"><a class="page-link">
                                ${i} <span class="sr-only">(current)</span></a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="page-item"><a class="page-link"
                                                 href="dishes?currentPage=${i}">${i}</a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>

            <c:if test="${currentPage lt noOfPages}">
                <li class="page-item"><a class="page-link"
                                         href="dishes?currentPage=${currentPage+1}">Вперед</a>
                </li>
            </c:if>
        </ul>
    </nav>
</div>

