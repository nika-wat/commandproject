<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="header-h2">
    <h2>Изменить продукт</h2>
</div>
<br>

<form method="post" enctype="multipart/form-data">
    <input type="hidden" value="${id}" name="id"/>
    <div class="col-6">
        <label for="title">Наименование</label>
        <input required name="title" type="text" class="form-control" id="title" value="${title}"/>
    </div>
    <div class="col-6">
        <label>Категория</label>
        <select id="inputState" class="form-control" required name="category">
            <option selected disabled>Выберите категорию</option>
            <c:forEach var="category" items="${categories}">
                <option value="${category}"
                        <c:if test="${categoriesValue == category}">selected</c:if>>${category.value}</option>
            </c:forEach>
        </select>
    </div>
    <div class="col-6">
        <label for="calories">Калории</label>
        <input required name="calories" type="number" class="form-control" id="calories" value="${calories}"/>
    </div>
    <div class="col-6">
        <label for="protein">Белки</label>
        <input required name="protein" type="number" class="form-control" id="protein" value="${protein}"/>
    </div>
    <div class="col-6">
        <label for="fats">Жиры</label>
        <input required name="fats" type="number" class="form-control" id="fats" value="${fats}"/>
    </div>
    <div class="col-6">
        <label for="carbohydrates">Углеводы</label>
        <input required name="carbohydrates" type="number" class="form-control" id="carbohydrates"
               value="${carbohydrates}"/>
    </div>
    <div class="col-6">
        <label <%--for="dataFile"--%>>Фото</label>

        <div class="m-1 ">
            <input type="file" name="dataFile">
        </div>
        <%--<input required name="photo" type="file" class="form-control" id="photo"
               accept=".jpg, .jpeg, .png"/>--%>
    </div>
<%--    <div class="col-6">--%>
<%--        <label for="photo">Фото</label>--%>
<%--        <input required name="photo" type="file" class="form-control" id="photo" value="${dataFile}"--%>
<%--               accept=".jpg, .jpeg, .png"/>--%>
<%--    </div>--%>
    <br>
    <button type="submit" class="btn btn-success">Сохранить</button>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
