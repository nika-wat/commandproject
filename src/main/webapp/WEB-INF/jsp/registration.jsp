<%--
  Created by IntelliJ IDEA.
  User: Alex
  Date: 29.01.2020
  Time: 19:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Registration Page</title>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="../../css/signin.css" >
    <meta name="theme-color" content="#563d7c">
</head>
<body class="bg-light text-dark">
<form class="form-signin" method="post" action="${pageContext.request.contextPath}/user/registration">

    <img class="rounded mx-auto d-block" src="../../img/logo.jpg" alt="" width="100" height="100">
    <h1 class="h3 mb-3 font-weight-normal text-center">Введите данные для регистрации</h1>

    <c:if test="${message != null}">
        <div class="alert alert-danger text-center" role="alert">${message}</div>
    </c:if>

    <label for="inputEmail" class="sr-only">Email адрес</label>
    <input type="text" name="username" id="inputEmail" class="form-control col-12 mx-auto"
           placeholder="Email address" required autofocus>

    <label for="password" class="sr-only">Пароль</label>
    <input type="password" name="password" id="password" class="form-control col-12 mx-auto"
           placeholder="Password" required>

    <button class="mt-3 btn btn-lg btn-primary btn-block col-10 mx-auto" type="submit" >Зарегистрироваться</button>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
</body>
</html>
