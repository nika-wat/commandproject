<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="container">
    <div class="row rounded justify-content-center ">
        <div class="col-10">

            <div class="header-h2">
                <h2>Показать продукт</h2>
            </div>
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-1" style="text-align:center">
                        <form method="get"
                              action='<c:url value="${pageContext.request.contextPath}/ingredients/${id}"/>'
                              style="display:inline">
                            <%--    <input type="hidden" name="id" value="<c:out value="${id}"/>">--%>
                            <input type="submit" class="btn btn-outline-warning btn-sm" value="Изменить">
                            <input type="text" hidden name="editable" value="true"/>
                            <%--    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>--%>
                        </form>
                    </div>
                    <div class="col-2" style="text-align:center">
                        <form method="post"
                              action='<c:url value="${pageContext.request.contextPath}/ingredients/${id}/delete"/>'
                              style="display:inline">
                            <input type="hidden" name="id" value="${id}">
                            <input type="submit" class="btn btn-outline-danger btn-sm" value="Удалить">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        </form>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <img src='<c:url value="${pageContext.request.contextPath}/${dataFile}"/>'
                             class="rounded mx-auto d-block" width="100%">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        Наименование
                    </div>
                    <div class="col-6">
                        ${title}
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        Категория
                    </div>
                    <div class="col-6">
                        ${categoriesName}
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        Калории
                    </div>
                    <div class="col-6">
                        ${calories}
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        Белки
                    </div>
                    <div class="col-6">
                        ${protein}
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        Жиры
                    </div>
                    <div class="col-6">
                        ${fats}
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        Углеводы
                    </div>
                    <div class="col-6">
                        ${carbohydrates}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>