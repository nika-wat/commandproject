<%--
  Created by IntelliJ IDEA.
  User: Alex
  Date: 29.01.2020
  Time: 20:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="center">
    <form method="post" action="${pageContext.request.contextPath}/user/info"  enctype="multipart/form-data">

        <c:if test="${user.photo != null}">
            <div class="form-group col-md  justify-content-center">
                <img src="../../${user.photo}" class="rounded mx-auto d-block" alt="..."
                     style="width: 25rem">
            </div>
        </c:if>
        <div class="m-1">
            <input type="file" name="dataFile">
        </div>

        <div class="form-row justify-content-center">
            <div class="form-group col-md">
                <label for="inputFullName">ФИО пользователя</label>
                <input type="text" name="inputFullName" class="form-control" id="inputFullName" value="${user.fullName}">
            </div>
            <div class="form-group col-md">
                <label for="inputGender">Пол</label>
                <select name="inputGender" id="inputGender" class="form-control">
                    <option selected>${user.gender}</option>
                    <c:forEach var="gender" items="${genders}">
                        <c:if test="${gender != user.gender}">
                            <option>${gender}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </div>
        </div>

        <div class="form-row justify-content-center">
            <div class="form-group col-md">
                <label for="inputBirthDate">Дата рождения</label>
                <input type="date" name="inputBirthDate" class="form-control" id="inputBirthDate" value="${user.birthDate}">
            </div>
            <div class="form-group col-md">
                <label for="inputFullAge">Полных лет</label>
                <input type="text" class="form-control" id="inputFullAge" value="${year}" readonly>
            </div>
        </div>

        <div class="form-row  justify-content-center">
            <div class="form-group col-md">
                <label for="inputHeight">Рост</label>
                <input type="number" name="inputHeight" class="form-control" id="inputHeight" value="${user.height}">
            </div>
            <div class="form-group col-md">
                <label for="inputWeight">Вес</label>
                <input type="text" name="inputWeight" class="form-control" id="inputWeight" value="${user.weight}">
            </div>
        </div>

        <div class="form-row  justify-content-center">
            <div class="form-group col-md">
                <label for="inputPurpose">Цель</label>
                <select name="inputPurpose"  id="inputPurpose" class="form-control">
                    <option selected>${user.purpose}</option>
                    <c:forEach var="purpose" items="${purposes}">
                        <c:if test="${purpose != user.purpose}">
                            <option>${purpose}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group col-md">
                <label for="inputActivity">Активность</label>
                <select name="inputActivity"  id="inputActivity" class="form-control">
                    <option selected>${user.activity}</option>
                    <c:forEach var="activity" items="${activities}">
                        <c:if test="${activity != user.activity}">
                            <option>${activity}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group col-md">
                <label for="inputNormCalories">Норма калорий</label>
                <input type="text" class="form-control" id="inputNormCalories" value="${user.normCalories}" readonly>
            </div>
        </div>

        <button type="submit" class="btn btn-primary" name="form" value="update">Сохранить</button>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>

    <form method="post" action="${pageContext.request.contextPath}/user/change-password">
        <div class="form-row  justify-content-center">
            <div class="form-group col-md">
                <label for="inputNewPassword">Новый пароль</label>
                <input name="inputNewPassword" type="password" class="form-control" id="inputNewPassword">
            </div>
            <div class="form-group col-md">
                <label for="inputOldPassword">Старый пароль</label>
                <input name="inputOldPassword" type="password" class="form-control" id="inputOldPassword">
            </div>
        </div>

        <div class="form-row  justify-content-center">
            <c:if test="${messageError != null}">
                <div class="alert alert-danger" role="alert">${messageError}</div>
            </c:if>
            <c:if test="${changePassword}">
                <div class="alert alert-info" role="alert">Пароль успешно изменён!</div>
            </c:if>
        </div>

        <button type="submit" class="btn btn-primary" name="form" value="changePassword">Изменить пароль</button>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div>
