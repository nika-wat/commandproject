<%--
  Created by IntelliJ IDEA.
  User: Alex
  Date: 28.01.2020
  Time: 22:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Login Page</title>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="../../css/signin.css" >
    <meta name="theme-color" content="#563d7c">
</head>
<body class="bg-light text-dark">
<form class="form-signin" method="post" action="${pageContext.request.contextPath}/user/login">

    <img class="rounded mx-auto d-block" src="../../img/logo.jpg" alt="" width="100" height="100">

    <h1 class="h3 mb-3 font-weight-normal text-center">Пожалуйста, войдите</h1>

    <c:if test="${logout}">
        <div class="alert alert-info" role="alert">Вы успешно вышли.</div>
    </c:if>
    <c:if test="${error}">
        <div class="alert alert-danger" role="alert">Неверный логин или пароль!</div>
    </c:if>

    <input type="text" name="email" id="inputEmail" class="form-control col-12 mx-auto"
           placeholder="Email address" value="${email}" required autofocus>

    <input type="password" name="password" id="inputPassword" class="form-control col-12 mx-auto"
           placeholder="Password" required>

    <div class="form-row">
        <button class="mt-3 btn btn-lg btn-primary btn-block col-4 mx-auto" type="submit" >Войти</button>

        <a class="mt-3 btn btn-lg btn-primary btn-block col-6 mx-auto"
           href="${pageContext.request.contextPath}/user/registration" role="button">Регистрация</a>
    </div>

    <p class="mt-3 mb-3 text-muted text-center">&copy; 2020</p>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
</body>
</html>
