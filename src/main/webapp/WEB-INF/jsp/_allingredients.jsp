


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row col ml-3">
    <h4>Список продуктов</h4><br>
</div>
<br>

<%--filter--%>
<div class="container">
    <div class="row rounded justify-content-center ">
        <div class="col-10">
            <form method="get" action="${pageContext.request.contextPath}/ingredients">
                <div class="form-group row">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend"><span class="input-group-text">Описание</span></div>
                        <input type="text" class="form-control" name="descriptionText"
                               value="<c:out value="${descriptionTextFilter}"/>">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend"><span class="input-group-text">Категория</span></div>
                        <select id="inputState" class="form-control" name="category">
                            <option value="${categoryFilterName}"><c:out value="${categoryFilter}"/></option>
                            <c:if test="${categoryFilter.length() > 1 }">
                                <option></option>
                            </c:if>
                            <c:set var="categories" value="${categories}"/>
                            <c:forEach var="category" items="${categories}">
                                <option value="${category}">${category.value}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>


                <div class="form-group row">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend"><span class="input-group-text">Число калорий</span></div>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <div class="input-group-prepend"><span class="input-group-text">min</span>
                        </div>
                        <input class="form-control" type="number" step="00000" min="0" placeholder="00000"
                               name="caloriesMin" value="<c:out value="${caloriesMinFilter}"/>">
                        &nbsp;-&nbsp;
                        <div class="input-group-prepend"><span class="input-group-text">max</span>
                        </div>
                        <input class="form-control" type="number" step="00000" min="0" placeholder="00000"
                               name="caloriesMax" value="<c:out value="${caloriesMaxFilter}"/>">

                    </div>
                </div>

                <div class="form-group row justify-content-end">
                    <div class="col-2">
                        <button type="submit" class="btn btn-primary">Выбрать</button>
                    </div>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
        </div>
    </div>
</div>
<%--filter--%>

<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Наименование</th>
        <th scope="col">Категория</th>
        <th scope="col">Калории</th>
        <th scope="col">Белки</th>
        <th scope="col">Жиры</th>
        <th scope="col">Углеводы</th>
        <th scope="col"></th>
    </tr>
    </thead>

    <tbody>
    <c:forEach var="ingredient" items="${ingredientList}">
    <tr>
        <td>${ingredient.getTitle()}</td>
        <td>${ingredient.getCategory().getValue()}</td>
        <td>${ingredient.getCalories()}</td>
        <td>${ingredient.getProtein()}</td>
        <td>${ingredient.getFats()}</td>
        <td>${ingredient.getCarbohydrates()}</td>
        <td>

            <div class="col-sm-4">
                <a href="${pageContext.request.contextPath}/ingredients/${ingredient.getIngredientId()}?editable=false"
                   class="btn btn-outline-warning btn-sm">Показать</a>
            </div>
        </td>
    </tr>
    </c:forEach>
    <tbody>
</table>

<div class="col-sm-4">
    <a href="${pageContext.request.contextPath}/ingredients/add" class="btn btn-info add-new">Добавить продукт</a>
</div>

<nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
        <c:set var="counter" value="${0}"/>
        <c:forEach var="pagination" items="${numberPagination}">

            <li class="page-item rounded shadow p-2 mb-3">
                <a class="page-link"
                   href="${pageContext.request.contextPath}/ingredients?page=<c:out value="${counter}"/>"><c:out
                        value="${counter}"/></a></li>
            <c:set var="counter" value="${counter + 1}"/>
        </c:forEach>
    </ul>
</nav>





