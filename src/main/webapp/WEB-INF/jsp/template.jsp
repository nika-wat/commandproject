<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><tiles:insertAttribute name="title"/></title>
    <style type="text/css">
        .header-h2 {
            position: relative;
            margin-bottom: .5rem;
        }

        .header-h2 h2 {
            font-size: 1.5rem;
            margin-bottom: 0;
            padding-left: 3rem;
            color: #007bff;
        }

        .header-h2 h2::before {
            content: "";
            position: absolute;
            left: 0;
            top: 0;
            width: 2.25rem;
            height: 2.25rem;
            background: #007BFF;
        }

        .header-h2 h2::after {
            content: "";
            position: absolute;
            left: 0;
            top: 2rem;
            width: 100%;
            height: .25rem;
            background-color: #007BFF;
        }
        #header1 {
            position: fixed;
            width: 100%; /* Ширина слоя */
            z-index: 1000;
            /*text-align: justify;*/
        }
        #sidebar {
            margin-top: 10px;
            width: 110px; /* Ширина слоя */
            padding: 0 10px; /* Отступы вокруг текста */
            float: left; /* Обтекание по правому краю */
        }
        #content {
            margin-top: 60px;
        }
        #content1 {
            margin-top: 60px;
        }
        #footer {
            position: fixed;


            left: 0;
            bottom: 0; /* Левый нижний угол */
            /* padding: 10px; !* Поля вокруг текста *!*/
            background: #007BFF; /* Цвет фона */
            color: #fff; /* Цвет текста */
            width: 100%; /* Ширина слоя */
            height: 70px;
        }

        .nav-primary {
            background: #ffee0f;
        }

        .blog-header-logo {
            color: #0c1232;
            font-size: 2.50rem;
        }

        .text-muted {
            color: #0c1232;
            font-size: 0.75rem;
        }
    </style>
    <link href="<c:out value="${path_js_css}"/>css/bootstrap.min.css" rel="stylesheet"/>
</head>

<body>
<script src="<c:out value="${path_js_css}"/>js/jquery-latest.js"></script>
<script src="<c:out value="${path_js_css}"/>bootstrap.js"></script>

<!--<div class="container">-->

<header class="container-fluid nav-primary">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="col-4 pt-1">
            <span class="text-muted">“СКАЖИ МНЕ, ЧТО ТЫ ЕШЬ, И Я СКАЖУ СКОЛЬКО ЭТО КАЛОРИЙ”</span>
        </div>
        <div class="col-4 text-center">
            <span class="blog-header-logo text-dark">DIET</span>
        </div>
        <div class="col-4 d-flex justify-content-end align-items-center">
            <c:if test="${authorized == true}">
                <div class="media"><img src="<c:out value="${user_photo}"/>" class="mr-3 rounded-circle" alt="..."
                                        style="width: 3rem; height: 3rem">
                </div>
                <div class="text-muted"><a href="${pageContext.request.contextPath}/user/info"><c:out
                        value="${user_name}"/></a></div>
                <div> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;</div>
                <form class="form-inline my-2 my-lg-0">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Выход</button>
                </form>
            </c:if>
            <c:if test="${authorized == false}">
                <a class="btn btn-outline-success my-2 my-sm-0"
                   href="${pageContext.request.contextPath}/user/login">Вход</a>
            </c:if>
        </div>
    </div>
</header>

<main role="main" class="container-fluid">
    <div class="row">
        <div class="col-2" id="content">
            <div id="sidebar">
                <div class="card" style="width: 14rem;">
                    <img src="<c:out value="${path_js_css}"/>img/ingr.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <a href="${pageContext.request.contextPath}/ingredients" class="card-title"
                           style="width: 10rem"><h5 class="card-title">Продукты</h5></a>
                    </div>
                </div>
                <div class="card" style="width: 14rem;">
                    <img src="<c:out value="${path_js_css}"/>img/fish.jpg" class="card-img-top" alt="...">
                    <div class="card-body">

                        <a href="${pageContext.request.contextPath}/dishes" class="card-title" style="width: 10rem"><h5
                                class="card-title">Блюда</h5></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-10" id="content1"><tiles:insertAttribute name="body"/></div>
    </div>
</main>

<footer class="container">
    <p style="text-align: center">©Zhdanov TEAM 2020</p>
</footer>

</body>
</html>

