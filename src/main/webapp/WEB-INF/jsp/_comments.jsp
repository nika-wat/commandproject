<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>

<c:set var="dish" value="${dish}"/>
<c:set var="dishId" value="${dish.getDishId()}"/>
<div class="row rounded justify-content-center ">
    <div class="col-10">
        <c:if test="${authorized}">
            <form method="post"
                  action="${pageContext.request.contextPath}/dishes/<c:out value="${dishId}"/>/comments">

                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Написать комментарий</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"
                              name="textComment"><c:out value="${textComment}"/></textarea>
                </div>
                <button type="submit" class="btn btn-primary rounded shadow p-2 mb-3 ">Отправить</button>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
        </c:if>
    </div>


</div>
<div class="row rounded justify-content-center  ">
    <div class="col-10 border bg-light">

        <%--<c:set var="comments" scope="page" value="${commentsList}"/>--%>
        <c:set var="counter" scope="page" value="${0}"/>
        <c:set var="idUser" scope="page"
               value="${pageContext.request.getAttribute('idUser')}"/>
        <c:forEach var="comment" items="${commentsList}">
            <c:if test="${comment.getIdUser() != idUser}">
                <div class="row mx-lg-n1">
                    <div class="col-11 border bg-light offset-md-0 rounded shadow p-2 mb-3 ">
                        <div class="col-12  border bg-white rounded  shadow p-2 mb-3 ">
                            <c:out value="${comment.getText()}"/>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col-3"><javatime:format value="${comment.getCreationDate()}"
                                                                pattern="yyyy-MM-dd HH:mm:ss"/></div>
                            <div class="col-3"><c:out value="${comment.getUser().getFullName()}"/></div>
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${comment.getIdUser() == idUser}">
                <div class="row  mx-lg-n1 ">
                    <div class="col-11 border bg-light offset-md-1 rounded shadow p-2 mb-3 ">
                        <div class="col-12  border bg-white rounded  shadow p-2 mb-3 ">
                            <c:out value="${comment.getText()}"/>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col-3"><c:out value="${comment.getCreationDate()}"/></div>
                            <div class="col-3"><c:out value="${comment.getUser().getFullName()}"/></div>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:set var="counter" scope="page" value="${counter + 1}"/>
        </c:forEach>


        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
                <c:set var="counter" scope="page" value="${0}"/>
                <c:set var="paginations" scope="page"
                       value="${pageContext.request.getAttribute('numberPagination')}"/>
                <c:set var="idSubj" scope="page"
                       value="${pageContext.request.getAttribute('dish').getDishId()}"/>
                <c:forEach var="pagination" items="${paginations}">
                    <li class="page-item rounded shadow p-2 mb-3">
                        <a class="page-link"
                           href="/dishes/<c:out value="${idSubj}"/>?editable=false&page=<c:out value="${pagination}"/>&size=10">
                            <c:out value="${pagination+1}"/></a></li>
                </c:forEach>
            </ul>
        </nav>

    </div>
</div>

