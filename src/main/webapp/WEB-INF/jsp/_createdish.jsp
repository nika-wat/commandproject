<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="dish" scope="session" value="${pageContext.request.getAttribute('dish')}"/>
<c:set var="dishId" scope="session" value="${dish.getDishId()}"/>
<div class="container" >
    <div class="row" >
        <div class="col-12">
            <%--<c:if test="${dish.getDishId() > 0}">--%>
            <%--<form method="post" action="${pageContext.request.contextPath}/newdish/exit" class="border border-white " style="border-radius: 10px">

                <div class="container m-1">
                    <div class="m-2" align="right">
                        <button type="submit" class="btn btn-primary" onclick="">Выйти из режима редактирования
                        </button>
                    </div>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>--%>
            <%--</c:if>--%>
            <form method="post" action="${pageContext.request.contextPath}/newdish/burn/<c:out value="${dishId}"/>" enctype="multipart/form-data" class="border border-white "
                  style="background-color:#CCCCCC; border-radius: 10px">
                <div class="container m-1">
                    <div class="row ">
                        <div class="col-12 input-group mb-3">
                            <div class="input-group-prepend"><span class="input-group-text">Наименование блюда</span>
                            </div>
                            <input class="form-control" type="text" value="<c:out value="${dish.getDishName()}"/>"
                                   name="dishName">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <div class="card" style="width: 17rem; height: 16rem">

                                <img src="<c:out value="${path_js_css}"/><c:out value="${dish.getPhoto()}"/>" class="rounded float-left" alt="..."
                                     style="width: 17rem; height: 16rem" name="img"></div>
                            <div class="m-1 ">
                                <input type="file" name="dataFile">
                            </div>
                        </div>
                        <div class="col-9">
                            <label for="exampleFormControlTextarea1">Рецепт</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="10" name="recipe"><c:out value="${dish.getRecipe()}"/>
                            </textarea>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-12 input-group mb-3">
                            <div class="input-group-prepend"><span class="input-group-text">Теги поиска</span></div>
                            <input class="form-control" type="text" value="<c:out value="${dish.getTagsString()}"/>"
                                   name="tedsSearch">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 input-group mb-3">
                            <div class="input-group-prepend"><span class="input-group-text">Калории</span></div>
                            <input class="form-control" type="text" step="00000.01" min="0" placeholder="00000,00"
                                   id="id05" name="calories" disabled style="background-color:#FFFFFF"
                                   value="<c:out value="${dish.getCalories()}"/>">
                        </div>
                        <div class="col-3 input-group mb-3">
                            <div class="input-group-prepend"><span class="input-group-text">Белки</span></div>
                            <input class="form-control" type="text" step="00000.01" min="0" placeholder="00000,00"
                                   name="protein" disabled style="background-color:#FFFFFF"
                                   value="<c:out value="${dish.getProtein()}"/>">
                        </div>
                        <div class="col-3 input-group mb-3">
                            <div class="input-group-prepend"><span class="input-group-text">Жиры</span></div>
                            <input class="form-control" type="text" step="00000.01" min="0" placeholder="00000,00"
                                   name="fat" disabled style="background-color:#FFFFFF"
                                   value="<c:out value="${dish.getFat()}"/>">
                        </div>
                        <div class="col-3 input-group mb-3">
                            <div class="input-group-prepend"><span class="input-group-text">Углеводы</span></div>
                            <input class="form-control" type="text" step="00000.01" min="0" placeholder="00000,00"
                                   name="carbohydrate" disabled style="background-color:#FFFFFF"
                                   value="<c:out value="${dish.getCarbohydrate()}"/>">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-3 input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Калории (на 100 гр.)</span>
                            </div>
                            <input class="form-control" type="text" min="0" placeholder="0"
                                   name="calories" disabled style="background-color:#FFFFFF" readonly
                                   value="<c:out value="${map.get('calories100')}"/>">
                        </div>
                        <div class="col-3 input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Белки (на 100 гр.)</span>
                            </div>
                            <input class="form-control" type="text" min="0" placeholder="0"
                                   name="protein" disabled style="background-color:#FFFFFF" readonly
                                   value="<c:out value="${map.get('protein100')}"/>">
                        </div>
                        <div class="col-3 input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Жиры (на 100 гр.)</span>
                            </div>
                            <input class="form-control" type="text" min="0" placeholder="0"
                                   name="fat" disabled style="background-color:#FFFFFF" readonly
                                   value="<c:out value="${map.get('fat100')}"/>">
                        </div>
                        <div class="col-3 input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Углеводы (на 100 гр.)</span>
                            </div>
                            <input class="form-control" type="text" min="0" placeholder="0"
                                   name="carbohydrate" disabled style="background-color:#FFFFFF" readonly
                                   value="<c:out value="${map.get('carbohydrate100')}"/>">
                        </div>
                    </div>

                    <div class="row justify-content-between">
                        <div class="col-5 input-group mb-3">
                            <div class="input-group-prepend"><span
                                    class="input-group-text">Время приготовления (мин.)</span></div>
                            <input class="form-control" type="number" step="00000" min="0" placeholder="00000"
                                   value="<c:out value="${dish.getCookingTime()}"/>" name="cookingTime">
                        </div>
                        <div class="m-2" align="right">
                            <button type="submit" class="btn btn-primary" value="true" name="writeDish">Записать
                            </button>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
            <div class="container">
                <br/>
            </div>
            <c:if test="${dishId > 0}">
                <div class="container " style="border: 2px solid #CCCCCC; border-radius: 10px">
                    <div class="container m-1">
                        <form method="post" action="${pageContext.request.contextPath}/newdish/categories/<c:out value="${dishId}"/>">
                            <div class="form-group row col-12">
                                <div class="col-10 input-group mb-3">
                                    <div class="input-group-prepend"><span class="input-group-text">Категории</span>
                                    </div>
                                    <select id="inputCategory" class="form-control" name="category">
                                        <option value="<c:out value="${currentCategoryName}"/>"><c:out value="${currentCategory}"/></option>
                                        <c:forEach var="category" items="${categories}">
                                            <option value="<c:out value="${category}"/>"><c:out
                                                    value="${category.value}"/></option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="form-group row justify-content-end">
                                    <div class="col-2">
                                        <button type="submit" class="btn btn-primary" name="select" style="width:6rem">
                                            Выбрать
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                        </form>
                        <form method="post" action="${pageContext.request.contextPath}/newdish/addingredient/<c:out value="${dishId}"/>">
                            <div class="form-group row col-12">
                                <div class="col-7 input-group mb-3">
                                    <div class="input-group-prepend"><span class="input-group-text">Ингредиенты</span>
                                    </div>
                                    <select id="inputIngredient" class="form-control" name="ingredient">
                                        <option></option>
                                        <c:forEach var="ingredient" items="${listIngredients}">
                                            <option value="<c:out value="${ingredient.getIngredientId()}"/>">
                                                <c:out value="${ingredient.getTitle()}"/></option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="col-3 input-group mb-3">
                                    <div class="input-group-prepend"><span class="input-group-text">Вес в гр.</span>
                                    </div>
                                    <input class="form-control" type="number" step="00000" min="0" placeholder="00000"
                                           name="weightGrams">
                                </div>
                                <div class="form-group row justify-content-end">
                                    <div class="col-2">
                                        <button type="submit" class="btn btn-primary" name="add" style="width:6rem">
                                            Добавить
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                        </form>
                        <div class="row ">
                            <c:set var="ingredients" scope="session" value="${dish.getIngredients()}"/>
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">Ингредиенты</th>
                                    <th scope="col" style="width: 8rem;">Вес в гр.</th>
                                    <th scope="col" style="width: 6rem;"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:set var="counter" value="${1}"/>
                                <c:forEach var="ingredient" items="${ingredients}">
                                    <form method="post"
                                          action="${pageContext.request.contextPath}/newdish/<c:out value="${dishId}"/>/ingredient/<c:out value="${ingredient.getId()}"/>/<c:out value="${ingredient.getWeight()}"/>/delete">
                                        <tr id="<c:out value="${counter}"/>">
                                            <td><c:out value="${ingredient.getIngredient().getTitle()}"/></td>
                                            <td><c:out value="${ingredient.getWeight()}"/></td>
                                            <td id="25">
                                                <button type="submit" class="btn btn-primary"
                                                        value="<c:out value="${ingredient.getId()}"/>#<c:out value="${ingredient.getWeight()}"/>"
                                                        name="delete">Удалить
                                                </button>
                                            </td>
                                        </tr>
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                    </form>
                                    <c:set var="counter" value="${counter + 1}"/>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${dishId > 0}">
                <form method="post" action="${pageContext.request.contextPath}/newdish/<c:out value="${dishId}"/>/remove" class="border border-white " style="border-radius: 10px">

                    <div class="container m-1">
                        <div class="m-2" align="right">
                            <button type="submit" class="btn btn-primary" onclick="">Удалить блюдо
                            </button>
                        </div>
                    </div>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                </form>
            </c:if>
        </div>
    </div>
</div>


